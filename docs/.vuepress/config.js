module.exports = {
    base:"/vuepress_blog/",
    title: '建鹏的博客',
    description: '我的学习总结',
    dest: './dist',
    port: '8080',
    head: [
        ['link', {rel: 'icon', href: '/img/logo.jpeg'}]
    ],
    markdown: {
        lineNumbers: true
    },
    themeConfig: {
        nav: require('./nav'),
        sidebar:require('./sidebar'),
        sidebarDepth: 2,
        lastUpdated: 'Last Updated',
        searchMaxSuggestoins: 10,
        serviceWorker: {
            updatePopup: {
                message: "New content is available.",
                buttonText: 'Refresh'
            }
        },
        editLinks: true,
        editLinkText: '在 GitHub 上编辑此页 ！'
    }
}


