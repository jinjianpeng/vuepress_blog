module.exports = {
	'/technical-summary/git/': require('../technical-summary/git/sidebar'),
	'/interview/network':require('../interview/network/sidebar'),
	'/technical-summary/javascript/':require('../technical-summary/javascript/sidebar'),
	'/technical-summary/css/':require('../technical-summary/css/sidebar'),
	'/interview/javascript/':require('../interview/javascript/sidebar'),
	'/interview/vue':require('../interview/vue/sidebar'),
	'/technical-summary/elementUI/':require('../technical-summary/elementUI/sidebar'),
	'/technical-summary/js提升/':require('../technical-summary/js提升/sidebar'),
	'/technical-summary/js易错知识回顾/':require('../technical-summary/js易错知识回顾/sidebar'),
}	
