module.exports = [
    {
        text: '首页', link: '/'
    },
    {
        text: '技术总结',
        items: [
            {
                text: 'git', link: '/technical-summary/git/'
            },
            {
                text: 'javascript', link: '/technical-summary/javascript/'
            },
            {
                text: 'element-ui', link: '/technical-summary/elementUI/'
            },
            {
                text: 'mongo', link: '/technical-summary/mongo/'
            },
            {
                text: 'ubuntu', link: '/technical-summary/ubuntu/'
            },
            {
                text: 'nodejs', link: '/technical-summary/node/'
            },
            {
                text: 'css', link: '/technical-summary/css/'
            },
            {
                text: 'github', link: '/technical-summary/github/'
            },
            {
                text: 'es6', link: '/technical-summary/es6/'
            },
            {
                text:'js提升',link:'/technical-summary/js提升/'
            },
            {
                text:'js易错知识回顾',link:'/technical-summary/js易错知识回顾/'
            }

        ]
    },
    {
        text: '面试题', 
        items: [ {
                text: '网络', link: '/interview/network/'
            },
            {
                text: 'javascript', link: '/interview/javascript/'
            },
            {
                text: 'vue', link: '/interview/vue/'
            },
        ]
    },
    {
        text: '优秀博客',
        items: [
            {
                text: '张鑫旭-鑫空间-鑫生活', link: 'https://www.zhangxinxu.com/'
            },{
                text:'文渊博客',link:'https://www.wenyuanblog.com/'
            },
            {
                text:'阮一峰',link:'http://www.ruanyifeng.com/blog/'
            },
            {
                text:'nodejs入门学习',link:'https://www.nodejs.red/#/'
            },
            {
                text:'TypeScript入门教程',link:'https://ts.xcatliu.com/'
            }
        ]
    },
    {
        text: '推荐电影', link: '/movie/'
    },
    {
    	text: '日记', link: '/diary/'
    },
    {
        text: '个人主页',
        items: [
            {
                text: 'GitHub', link: 'https://github.com/janzenkin'
            },
            {
                text: '掘金', link: 'https://juejin.im/'
            },
            {
                text: '简书', link: 'https://www.jianshu.com/'
            },
            {
                text: 'CSDN', link: 'https://blog.csdn.net/qq_38527673?spm=1010.2135.3001.5113'
            },
            
            {
                text: '知乎', link: 'https://www.zhihu.com/people/jin-jian-peng-55'
            },
            {
                text: 'segmentfault', link: 'https://segmentfault.com'
            },
            
            {
                text: 'v2ex', link: 'https://www.v2ex.com/'
            }
        ]
    }
]


