### 函数参数默认值和arguments

```javascript
function side(arr) {
  arr[0] = arr[2];
}
function a(a, b, c = 3) {
  c = 10;
  side(arguments);
  return a + b + c;
}
a(1, 1, 1);
```
**解析**

```javascript
function side(arr) {
  arr[0] = arr[2];
}
function a(a, b, c = 3) {
  c = 10;
  console.log(arguments);
  side(arguments);  // 这里 a，c的值不管怎么改变都是不会改变的
  return a + b + c;
}
a(1, 1, 1);  //12
```
但是，如果是
```javascript
function side(arr) {
  arr[0] = arr[2];
}
function a(a, b, c) {
  c = 10;
  console.log(arguments);
  side(arguments);  // 这里 a，c的值不管怎么改变都是不会改变的
  return a + b + c;
}
a(1, 1, 1);  // 21
```
> 加了默认值，则转为严格模式（其实也可以使用 "use strict" 转），这时候参数（a、b、c）与 arguments 没有绑定关系，所以修改 arguments 不会影响到参数（a、b、c）的值，修改参数（a、b、c）也不会影响到 arguments。
不加默认值，则为非严格模式，结果和上面的相反。

## 变量提升
```javascript
(function () {
  var a = (b = 5);
})();

console.log(b);
console.log(a);
```
**结果**
`5 Error, a is not defined`

在这个立即执行函数表达式（IIFE）中包括两个赋值操作，其中a使用var关键字进行声明，因此其属于函数内部的局部变量（仅存在于函数中），相反，b被分配到全局命名空间。
另一个需要注意的是，这里没有在函数内部使用严格模式(use strict;)。如果启用了严格模式，代码会在输出 b 时报错Uncaught ReferenceError: b is not defined,需要记住的是，严格模式要求你显式的引用全局作用域。


## 现代 Web 页面性能方面的最佳实践
 - 使用 HTTP/2

 - 使用 passive 的事件监听器

       addEventListener(type, listener, {
            capture: false,
            passive: false,
            once: false
        }) 
      - 三个属性都是布尔类型的开关，<font color="red">默认值都为 false</font>。其中 capture 属性等价于以前的 useCapture 参数；once 属性就是表明该监听器是一次性的，执行一次后就被自动 removeEventListener 掉，还没有浏览器实现它；passive 属性是本文的主角，Firefox 和 Chrome 已经实现
      
      - 默认行为可以被监听器通过 preventDefault() 方法阻止，那它的默认行为是什么呢，通常来说就是滚动当前页面（还可能是缩放页面），如果它的默认行为被阻止了，页面就必须静止不动。但浏览器无法预先知道一个监听器会不会调用 preventDefault()，它能做的只有等监听器执行完后再去执行默认行为，而监听器执行是要耗时的，有些甚至耗时很明显，这样就会导致页面卡顿。视频里也说了，即便监听器是个空函数，也会产生一定的卡顿，毕竟空函数的执行也会耗时。
      
      - 视频里还说了，有 80% 的滚动事件监听器是不会阻止默认行为的，也就是说大部分情况下，浏览器是白等了。所以，passive 监听器诞生了，passive 的意思是“顺从的”，表示它不会对事件的默认行为说 no，浏览器知道了一个监听器是 passive 的，它就可以在两个线程里同时执行监听器中的 JavaScript 代码和浏览器的默认行为了。
 - 静态文件使用 CDN
 - 尽量使用更先进的图片格式，比如 WebP


 ## 箭头函数
  - 箭头函数表达式的语法比函数表达式更简洁，并且<font color="red" style="font-weight:bold;">没有自己的this，arguments，super或new.target。</font>箭头函数表达式更适用于那些本来需要匿名函数的地方，并且它不能用作构造函数。

  - 1.通过 call 或 apply 调用

    - 由于 箭头函数没有自己的this指针，通过 call() 或 apply() 方法调用一个函数时，只能传递参数（不能绑定this---译者注），他们的第一个参数会被忽略。

  - 2.不绑定arguments
    
    - 箭头函数不绑定Arguments 对象。因此，在本示例中，arguments只是引用了封闭作用域内的arguments
    
- 3.使用 `new` 操作符

  - 箭头函数不能用作构造器，和 `new`一起用会抛出错误。

  - ```javascript
    var Foo = () => {};
    var foo = new Foo(); // TypeError: Foo is not a constructor
    ```




## 写出3个使用this的典型应用

（1）在html元素事件属性中使用，如：

```html
<input type=”button” onclick=”showInfo(this);” value=”点击一下”/>
```

（2）构造函数

```javascript
function Animal(name, color) {
　　this.name = name;
　　this.color = color;
}
```

（3）input点击，获取值

```javascript
<input type="button" id="text" value="点击一下" />
<script type="text/javascript">
    var btn = document.getElementById("text");
    btn.onclick = function() {
        alert(this.value);    //此处的this是按钮元素
    }
</script>
```

(4)apply()/call()求数组最值

```javascript
var  numbers = [5, 458 , 120 , -215 ]; 
var  maxInNumbers = Math.max.apply(this, numbers);  
console.log(maxInNumbers);  // 458
var maxInNumbers = Math.max.call(this,5, 458 , 120 , -215); 
console.log(maxInNumbers);  // 458
```

### 数组去重

```javascript
// 方法一
var norepeat = funtion(arr){
    return arr.filter(function(val, index, array){
        return array.indexOf(val) === index;
    });
}
norepeat()


// 方法二
var set = new Set(arr);
```

### 数组求和

```javascript
var sum = function(arr){
    return arr.reduce(function(x, y){
        return x + y
    });
}

sum()
```

### 补充代码，鼠标单击Button1后将Button1移动到Button2的后面.

```javascript
<div>
<input type="button" id ="button1" value="1" onclick="moveBtn(this);">
<input type="button" id ="button2" value="2" />
</div>
<script type="text/javascript">
function moveBtn(obj) {
var clone = obj.cloneNode(true);
var parent = obj.parentNode;
parent.appendChild(clone);
parent.removeChild(obj);
}
</script>
```

### JavaScript中如何对一个对象进行深度clone

方法一：

```javascript
function deepClone(obj){
  var str = JSON.sringify(obj);
  var newobj = JSON.parse(str);
  return newobj;
 }
```

深度克隆方法一通过JSON.stringfy比较简单，不过已经可以解决大部分使用场景，但也不得不说一些情况下还是会有问题的，比如
1.它无法实现对函数 、RegExp等特殊对象的克隆;
2.会抛弃对象的constructor,所有的构造函数会指向Object;
3.对象有循环引用,会报错;
所以用什么方法深度克隆还是根据实际情况来定的。

方法二：

```javascript
//深克隆
function deepClone(obj){
    if (!obj) { return obj; }
    var o = obj instanceof Array ? [] : {};
    for(var k in obj){
        if(obj.hasOwnProperty(k)){
            o[k] = typeof obj[k] === "object" ? deepClone(obj[k]) : obj[k];
        }
    }
    return o;
}
```

### 鼠标点击页面中的任意标签，alert该标签的名称．（注意兼容性)

```javascript
<script type="text/javascript">
   function elementName(evt){
    evt = evt|| window.event;
    var selected = evt.target || evt.srcElement;
    alert(selected.tagName);
   }
   
   window.onload = function(){
    var el = document.getElementsByTagName('body');
    el[0].onclick = elementName;
   }
  </script>
```

### 请编写一个JavaScript函数 parseQueryString，它的用途是把URL参数解析为一个对象

```javascript
var url = "http://witmax.cn/index.php?key0=0&key1=1&key2=2";


function parseQueryString(argu){
    var str = argu.split('?')[1];
    var result = {};
    var temp = str.split('&');
    for(var i=0; i<temp.length; i++){
        var temp2 = temp[i].split('=');
        result[temp2[0]] = temp2[1];
    }
    return result;
}
parseQueryString(url)
```

##  判断一个字符串中出现次数最多的字符，统计这个次数

> 方法一：利用json数据个数“键”唯一的特性
> 方法二、利用数组reduce()方法；同时应用一个函数针对数组的两个值(从左到右)。
> 方法三、利用正则表达式的replace对str的每一项进行检测



## 请描述一下 cookies sessionStorage和localstorage区别

相同点：都存储在客户端
不同点：

```
   1. 存储大小
       cookie数据大小不能超过4k。
       sessionStorage和localStorage 虽然也有存储大小的限制，但比cookie大得多，可以达到5M或更大。
   2. 有效时间
       localStorage：存储持久数据，浏览器关闭后数据不丢失除非主动删除数据；
       sessionStorage：数据在当前浏览器窗口关闭后自动删除。
       cookie：设置的cookie过期时间之前一直有效，即使窗口或浏览器关闭
   3. 数据与服务器之间的交互方式
       cookie的数据会自动的传递到服务器，服务器端也可以写cookie到客户端
       sessionStorage和localStorage不会自动把数据发给服务器，仅在本地保存。
```

## undefined 和 null 区别

```
javaScript权威指南：
null 和 undefined 都表示“值的空缺”，你可以认为undefined是表示系统级的、出乎意料的或类似错误的值的空缺，而null是表示程序级的、正常的或在意料之中的值的空缺。

javaScript高级程序设计：
在使用var声明变量但未对其加以初始化时，这个变量的值就是undefined。  null值则是表示空对象指针。
```

## 常见的HTTP状态码

- 2开头 （请求成功）表示成功处理了请求的状态代码。
- 3开头 （请求被重定向）表示要完成请求，需要进一步操作。 通常，这些状态代码用来重定向。
- 4开头 （请求错误）这些状态代码表示请求可能出错，妨碍了服务器的处理。
- 5开头（服务器错误）这些状态代码表示服务器在尝试处理请求时发生内部错误。 这些错误可能是服务器本身的错误，而不是请求出错



## 什么是mvvm mvc是什么区别 原理

MVC（Model-View-Controller）即模型-视图-控制器。
Model（模型）：
是应用程序中用于处理应用程序数据逻辑的部分。通常模型对象负责在数据库中存取数据。

```
Model定义了这个模块的数据模型。在代码中体现为数据管理者，Model负责对数据进行获取及存放。
数据不可能凭空生成的，要么是从服务器上面获取到的数据，要么是本地数据库中的数据，也有可能是用户在UI上填写的表单即将上传到服务器上面存放，所以需要有数据来源。既然Model是数据管理者，则自然由它来负责获取数据。
Controller不需要关心Model是如何拿到数据的，只管调用就行了。
数据存放的地方是在Model，而使用数据的地方是在Controller，
所以Model应该提供接口供controller访问其存放的数据（通常通过.h里面的只读属性）
```

View（视图）：
是应用程序中处理数据显示的部分。通常视图是依据模型数据创建的。

```
View，视图，简单来说，就是我们在界面上看见的一切。 它们有一部分是我们UI定死的，也就是不会根据数据来更新显示的， 比如一些Logo图片啊，这里有个按钮啊，那里有个输入框啊，一些显示特定内容的label啊等等； 有一部分是会根据数据来显示内容的，比如tableView来显示好友列表啊， 这个tableView的显示内容肯定是根据数据来显示的。 我们使用MVC解决问题的时候，通常是解决这些根据数据来显示内容的视图。
```

Controller（控制器）：是应用程序中处理用户交互的部分。通常控制器负责从视图读取数据，控制用户输入，并向模型发送数据。

```
Controller是MVC中的数据和视图的协调者，也就是在Controller里面把Model的数据赋值给View来显示（或者是View接收用户输入的数据然后由Controller把这些数据传给Model来保存到本地或者上传到
服务器）。
```

各部分之间的通信方式如下，所有通讯都是单向的 。

```
    1. View 传送指令到 Controller
    2. Controller 完成业务逻辑后，要求 Model 改变状态
    3. Model 将新的数据发送到 View，用户得到反馈
```





# 聊一聊valueOf和toString

>  valueOf和toString是Object.prototype的方法。一般很少直接调用，但是在使用对象参与运算的时候就会调用这两个方法了。我想大部分人都存在以下疑问：

- valueOf和toString哪个优先级较高?
- 是不是所有场景都会调用valueOf和toString

## 概念解释

- valueOf: 返回对象的原始值表示
- toString: 返回对象的字符串表示

### valueOf转换规则

说到原始值就必须说到原始类型，JS规范中 **原始类型** 如下：

- Boolean
- Null
- Undefined
- Number
- Strin

**不同类型对象的valueOf()方法的返回值**

| **对象** | **返回值**                                               |
| :------- | :------------------------------------------------------- |
| Array    | 返回数组对象本身。                                       |
| Boolean  | 布尔值。                                                 |
| Date     | 存储的时间是从 1970 年 1 月 1 日午夜开始计的毫秒数 UTC。 |
| Function | 函数本身。                                               |
| Number   | 数字值。                                                 |
| Object   | 对象本身。这是默认情况。                                 |
| String   | 字符串值。                                               |
|          | Math 和 Error 对象没有 valueOf 方法。                    |

**默认情况下，`valueOf`方法由[`Object`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object)后面的每个对象继承。 每个内置的核心对象都会覆盖此方法以返回适当的值。如果对象没有原始值，则`valueOf`将返回对象本身。**



### toString()转换规则

每个对象都有一个 `toString()` 方法，当该对象被表示为一个文本值时，或者一个对象以预期的字符串方式引用时自动调用。默认情况下，`toString()` 方法被每个 `Object` 对象继承。如果此方法在自定义对象中未被覆盖，`toString()` 返回 "[object *type*]"，其中 `type` 是对象的类型。

| 对象     | toString返回值                                               |
| -------- | ------------------------------------------------------------ |
| Array    | 以逗号分割的字符串，如[1,2]的toString返回值为"1,2"           |
| Boolean  | "True"                                                       |
| Date     | 可读的时间字符串，如"Tue Oct 15 2019 12:20:56 GMT+0800 (中国标准时间)" |
| Function | 声明函数的JS源代码字符串                                     |
| Number   | "数字值"                                                     |
| Object   | "[object Object]"                                            |
| String   | "字符串"                                                     |




> **注意：**如的ECMAScript 5 和随后的 Errata 中所定义，从 JavaScript 1.8.5 开始，`toString()` 调用 [`null`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/null) 返回`[object *Null*]`，[`undefined`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/undefined) 返回 `[object Undefined]`。请参阅下面的[使用 `toString()` 检测对象类型](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/toString#Using_toString()_to_detect_object_class)。

**使用 `toString()` 检测对象类**

可以通过 `toString()` 来获取每个对象的类型。为了每个对象都能通过 `Object.prototype.toString()` 来检测，需要以 `Function.prototype.call()` 或者 `Function.prototype.apply()` 的形式来调用，传递要检查的对象作为第一个参数，称为 `thisArg`。

```js
var toString = Object.prototype.toString;

toString.call(new Date); // [object Date]
toString.call(new String); // [object String]
toString.call(Math); // [object Math]

//Since JavaScript 1.8.5
toString.call(undefined); // [object Undefined]
toString.call(null); // [object Null]
```

- **加法：一个操作数是字符串，另一个操作数是对象或、数组、布尔值，则调用它们的toString（）方法取得相应的字符串值**

- **减法：如果一个操作数是对象，则调用对象的valueOf()方法取得表示该对象的数值，如果对象没有该方法，则调用其toString()方法**
- 关系操作：如果一个操作数是对象，则调用这个对象的valueOf(),没有该对象则调用toString()
- 相等操作符：**相等或不相等**---先转换再比较，**全等和不全等**---仅比较而不转换。<u>NAN！=NAN</u>

调用规则：

- 加法：根据操作数两者的值类型。如果是字符串则另一个调用toString()，是数值则另一个调用valueOf()
- 减法：不论数值还是字符串，另一个都调用valueOf()
- 相等：有数值调用valueof或对象，调用valueOf，如果两个数都是对象，则当两个操作数都指向同一个对象时，才为true否则都为false





## 垃圾回收

> 垃圾回收算法主要依赖于引用的概念。在内存管理的环境中，一个对象如果有访问另一个对象的权限（隐式或者显式），叫做一个对象引用另一个对象。例如，一个Javascript对象具有对它[原型](https://developer.mozilla.org/en/JavaScript/Guide/Inheritance_and_the_prototype_chain)的引用（隐式引用）和对它属性的引用（显式引用）。

**引用计数垃圾收集**

这是最初级的垃圾收集算法。此算法把“对象是否不再需要”简化定义为“对象有没有其他对象引用到它”。如果没有引用指向该对象（零引用），对象将被垃圾回收机制回收。

```js
var o = { 
  a: {
    b:2
  }
}; 
// 两个对象被创建，一个作为另一个的属性被引用，另一个被分配给变量o
// 很显然，没有一个可以被垃圾收集


var o2 = o; // o2变量是第二个对“这个对象”的引用

o = 1;      // 现在，“这个对象”只有一个o2变量的引用了，“这个对象”的原始引用o已经没有

var oa = o2.a; // 引用“这个对象”的a属性
               // 现在，“这个对象”有两个引用了，一个是o2，一个是oa

o2 = "yo"; // 虽然最初的对象现在已经是零引用了，可以被垃圾回收了
           // 但是它的属性a的对象还在被oa引用，所以还不能回收

oa = null; // a属性的那个对象现在也是零引用了
           // 它可以被垃圾回收了
```

#### 限制：循环引用

该算法有个限制：无法处理循环引用的事例。在下面的例子中，两个对象被创建，并互相引用，形成了一个循环。它们被调用之后会离开函数作用域，所以它们已经没有用了，可以被回收了。然而，引用计数算法考虑到它们互相都有至少一次引用，所以它们不会被回收。

```js
function f(){
  var o = {};
  var o2 = {};
  o.a = o2; // o 引用 o2
  o2.a = o; // o2 引用 o

  return "azerty";
}

f();
```

**标记-清除算法**

这个算法把“对象是否不再需要”简化定义为“对象是否可以获得”。

这个算法假定设置一个叫做根（root）的对象（在Javascript里，根是全局对象）。垃圾回收器将定期从根开始，找所有从根开始引用的对象，然后找这些对象引用的对象……从根开始，垃圾回收器将找到所有可以获得的对象和收集所有不能获得的对象。

这个算法比前一个要好，因为“有零引用的对象”总是不可获得的，但是相反却不一定，参考“循环引用”。

从2012年起，所有现代浏览器都使用了标记-清除垃圾回收算法。所有对JavaScript垃圾回收算法的改进都是基于标记-清除算法的改进，并没有改进标记-清除算法本身和它对“对象是否不再需要”的简化定义。

#### 循环引用不再是问题了

在上面的示例中，函数调用返回之后，两个对象从全局对象出发无法获取。因此，他们将会被垃圾回收器回收。第二个示例同样，一旦 div 和其事件处理无法从根获取到，他们将会被垃圾回收器回收。





## 数组

### 遍历数组

1. 最简单的方式：

```js
var colors = ['red', 'green', 'blue'];
for (var i = 0; i < colors.length; i++) {
  console.log(colors[i]);
}
```

2. `forEach()` 方法提供了遍历数组元素的其他方法

```js
var colors = ['red', 'green', 'blue'];
colors.forEach(function(color) {
  console.log(color);
});
```

<font color="red">**被传递给forEach的函数会在数组的每个元素像上执行一次，元素作为参数传递给该函数。未赋值的值不会在forEach循环迭代。**</font>

3. `for...in...` 方法

```js
var colors = ['red', 'green', 'blue'];
for(var index in colors){
	console.log(colors[index])
}
```

3. `for...of..`方法

```js
var colors = ['red', 'green', 'blue'];
for(var val of colors){
	console.log(val)
}
```

### 数组的方法

- concat

- join

- push

- pop

- shift

- unshift

- lastIndexOf

- indexOf

- slice

- splice

- split

- reverse

- sort

  **迭代**

- forEach

- map

- filter

- every

- some

- reduce

`concat()`连接两个数组并返回一个新的数组。

```js
var myArray = new Array("1", "2", "3");
myArray = myArray.concat("a", "b", "c"); 
// myArray is now ["1", "2", "3", "a", "b", "c"]
```

`join(deliminator = ',')`将数组的所有元素连接成一个字符串。

```js
var myArray = new Array("Wind", "Rain", "Fire");
var list = myArray.join(" - "); // list is "Wind - Rain - Fire"
```

[`push()`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/push) 在数组末尾添加一个或多个元素，并返回数组操作后的长度。

```js
var myArray = new Array("1", "2");
myArray.push("3"); // myArray is now ["1", "2", "3"]
```

[`pop()`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/pop) 从数组移出最后一个元素，并返回该元素。

```js
var myArray = new Array("1", "2", "3");
var last = myArray.pop(); 
// myArray is now ["1", "2"], last = "3"
```

[`shift()`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/shift) 从数组移出第一个元素，并返回该元素。

```js
var myArray = new Array ("1", "2", "3");
var first = myArray.shift(); 
// myArray is now ["2", "3"], first is "1"
```

[`unshift()`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/shift) 在数组开头添加一个或多个元素，并返回数组的新长度。

```js
var myArray = new Array ("1", "2", "3");
myArray.unshift("4", "5"); 
// myArray becomes ["4", "5", "1", "2", "3"]
```

[`slice(start_index, upto_index)`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/slice) 从数组提取一个片段，并作为一个新数组返回。

```js
var myArray = new Array ("a", "b", "c", "d", "e");
myArray = myArray.slice(1, 4); // 包含索引1，不包括索引4
                               // returning [ "b", "c", "d"]
```

[`splice(index, count_to_remove, addElement1, addElement2, ...)`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/splice)从数组移出一些元素，（可选）并替换它们。

```js
var myArray = new Array ("1", "2", "3", "4", "5");
myArray.splice(1, 3, "a", "b", "c", "d"); 
// myArray is now ["1", "a", "b", "c", "d", "5"]
// This code started at index one (or where the "2" was), 
// removed 3 elements there, and then inserted all consecutive
// elements in its place.
```

[`reverse()`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/reverse) 颠倒数组元素的顺序：第一个变成最后一个，最后一个变成第一个。

```js
var myArray = new Array ("1", "2", "3");
myArray.reverse(); 
// transposes the array so that myArray = [ "3", "2", "1" ]
```

[`sort()`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/sort) 给数组元素排序。

```js
var myArray = new Array("Wind", "Rain", "Fire");
myArray.sort(); 
// sorts the array so that myArray = [ "Fire", "Rain", "Wind" ]
```

`sort()` 也可以带一个回调函数来决定怎么比较数组元素。这个回调函数比较两个值，并返回3

例如，下面的代码通过字符串的最后一个字母进行排序：

```js
var sortFn = function(a, b){
  if (a[a.length - 1] < b[b.length - 1]) return -1;
  if (a[a.length - 1] > b[b.length - 1]) return 1;
  if (a[a.length - 1] == b[b.length - 1]) return 0;
}
myArray.sort(sortFn); 
// sorts the array so that myArray = ["Wind","Fire","Rain"]
```

- 如果 a 小于 b ，返回 -1(或任何负数)

- 如果 `a` 大于 `b` ，返回 1 (或任何正数)

- 如果 `a` 和 `b` 相等，返回 0。

`indexOf(searchElement[, fromIndex])`在数组中搜索searchElement 并返回第一个匹配的索引。

```js
var a = ['a', 'b', 'a', 'b', 'a'];
console.log(a.indexOf('b')); // logs 1
// Now try again, starting from after the last match
console.log(a.indexOf('b', 2)); // logs 3
console.log(a.indexOf('z')); // logs -1, because 'z' was not found
```

`lastIndexOf(searchElement[, fromIndex])`和 `indexOf 差不多，但这是从结尾开始，并且是反向搜索。`

```js
var a = ['a', 'b', 'c', 'd', 'a', 'b'];
console.log(a.lastIndexOf('b')); // logs 5
// Now try again, starting from before the last match
console.log(a.lastIndexOf('b', 4)); // logs 1
console.log(a.lastIndexOf('z')); // logs -1
```

`forEach(callback[, thisObject])` 在数组每个元素项上执行`callback`。

```js
var a = ['a', 'b', 'c'];
a.forEach(function(element) { console.log(element);} ); 
// logs each item in turn
```

`map(callback[, thisObject])`在数组的每个单元项上执行callback函数，并把返回包含回调函数返回值的新数组（译者注：也就是遍历数组，并通过callback对数组元素进行操作，并将所有操作结果放入数组中并返回该数组）。

```js
var a1 = ['a', 'b', 'c'];
var a2 = a1.map(function(item) { return item.toUpperCase(); });
console.log(a2); // logs A,B,C
```

`filter(callback[, thisObject])` 返回一个包含所有在回调函数上返回为true的元素的新数组（译者注：callback在这里担任的是过滤器的角色，当元素符合条件，过滤器就返回true，而filter则会返回所有符合过滤条件的元素）。

```js
var a1 = ['a', 10, 'b', 20, 'c', 30];
var a2 = a1.filter(function(item) { return typeof item == 'number'; });
console.log(a2); // logs 10,20,30
```

`every(callback[, thisObject])`当数组中每一个元素在callback上被返回true时就返回true（译者注：同上，every其实类似filter，只不过它的功能是判断是不是数组中的所有元素都符合条件，并且返回的是布尔值）。

```js
function isNumber(value){
  return typeof value == 'number';
}
var a1 = [1, 2, 3];
console.log(a1.every(isNumber)); // logs true
var a2 = [1, '2', 3];
console.log(a2.every(isNumber)); // logs false
```

`some(callback[, thisObject])`只要数组中有一项在callback上被返回true，就返回true（译者注：同上，类似every，不过前者要求都符合筛选条件才返回true，后者只要有符合条件的就返回true）。

```js
function isNumber(value){
  return typeof value == 'number';
}
var a1 = [1, 2, 3];
console.log(a1.some(isNumber)); // logs true
var a2 = [1, '2', 3];
console.log(a2.some(isNumber)); // logs true
var a3 = ['1', '2', '3'];
console.log(a3.some(isNumber)); // logs false
```

以上方法都带一个被称为迭代方法的的回调函数，因为他们以某种方式迭代整个数组。都有一个可选的第二参数 `thisObject`，如果提供了这个参数，`thisObject` 变成回调函数内部的 this 关键字的值。如果没有提供，例如函数在一个显示的对象上下文外被调用时，this 将引用全局对象([`window`](https://developer.mozilla.org/zh-CN/docs/Web/API/Window)).

实际上在调用回调函数时传入了3个参数。第一个是当前元素项的值，第二个是它在数组中的索引，第三个是数组本身的一个引用。 JavaScript 函数忽略任何没有在参数列表中命名的参数，因此提供一个只有一个参数的回调函数是安全的，例如 `alert` 。

`reduce(callback[, initialValue])`使用回调函数 `callback(firstValue, secondValue)` 把数组列表计算成一个单一值（译者注：他数组元素两两递归处理的方式把数组计算成一个值）

```js
var a = [10, 20, 30];
var total = a.reduce(function(first, second) { return first + second; }, 0);
console.log(total) // Prints 60
```

`reduceRight(callback[, initalvalue])`和 `reduce()相似，但这从最后一个元素开始的。`

`reduce` 和 `reduceRight` 是迭代数组方法中最不被人熟知的两个函数.。他们应该使用在那些需要把数组的元素两两递归处理，并最终计算成一个单一结果的算法。

### 多维数组(multi-dimensional arrays)



数组是可以嵌套的, 这就意味着一个数组可以作为一个元素被包含在另外一个数组里面。利用JavaScript数组的这个特性, 可以创建多维数组。

以下代码创建了一个二维数组。

```js
var a = new Array(4);
for (i = 0; i < 4; i++) {
  a[i] = new Array(4);
  for (j = 0; j < 4; j++) {
    a[i][j] = "[" + i + "," + j + "]";
  }
}
```

这个例子创建的数组拥有以下行数据:

```html
Row 0: [0,0] [0,1] [0,2] [0,3]
Row 1: [1,0] [1,1] [1,2] [1,3]
Row 2: [2,0] [2,1] [2,2] [2,3]
Row 3: [3,0] [3,1] [3,2] [3,3]
```

## 日期对象

JavaScript没有日期数据类型。但是你可以在你的程序里使用 [`Date`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Date) 对象和其方法来处理日期和时间。Date对象有大量的设置、获取和操作日期的方法。 它并不含有任何属性。

JavaScript 处理日期数据类似于Java。这两种语言有许多一样的处理日期的方法，也都是以1970年1月1日00:00:00以来的毫秒数来储存数据类型的。

创建一个日期对象：

```js
var dateObjectName = new Date([parameters]);
```

<font color="red">**不使用 *new* 关键字来调用Date对象将返回当前时间和日期的字符串**</font>

前边的语法中的参数（parameters）可以是一下任何一种：

- 无参数 : 创建今天的日期和时间，例如： `today = new Date();`.
- 一个符合以下格式的表示日期的字符串: "月 日, 年 时:分:秒." 例如： `var Xmas95 = new Date("December 25, 1995 13:30:00")。`如果你省略时、分、秒，那么他们的值将被设置为0。
- 一个年，月，日的整型值的集合，例如： `var Xmas95 = new Date(1995, 11, 25)。`
- 一个年，月，日，时，分，秒的集合，例如： `var Xmas95 = new Date(1995, 11, 25, 9, 30, 0);`.

 通过“get”和“set”方法，你可以分别设置和获取秒，分，时，日，星期，月份，年。这里有个getDay方法可以返回星期，但是没有相应的setDay方法用来设置星期，因为星期是自动设置的。这些方法用整数来代表以下这些值：

- 秒，分： 0 至 59
- 时： 0 至 23
- 星期： 0 (周日) 至 6 (周六)
- 日期：1 至 31 
- 月份： 0 (一月) to 11 (十二月)
- 年份： 从1900开始的年数

`getTime` 和 `setTime` 方法对于比较日期是非常有用的。`getTime`方法返回从1970年1月1日00:00:00的毫秒数。

例如，以下代码展示了今年剩下的天数：

```js
var today = new Date();
var endYear = new Date(1995, 11, 31, 23, 59, 59, 999); // 设置日和月，注意，月份是0-11
endYear.setFullYear(today.getFullYear()); // 把年设置为今年
var msPerDay = 24 * 60 * 60 * 1000; // 每天的毫秒数
var daysLeft = (endYear.getTime() - today.getTime()) / msPerDay;
var daysLeft = Math.round(daysLeft); //返回今年剩下的天数
```

### 例子：

在下边的例子中，JSClock()函数返回了用数字时钟格式的时间：

```js
function JSClock() {
  var time = new Date();
  var hour = time.getHours();
  var minute = time.getMinutes();
  var second = time.getSeconds();
  var temp = "" + ((hour > 12) ? hour - 12 : hour);
  if (hour == 0)
    temp = "12";
  temp += ((minute < 10) ? ":0" : ":") + minute;
  temp += ((second < 10) ? ":0" : ":") + second;
  temp += (hour >= 12) ? " P.M." : " A.M.";
  return temp;
}
JSClock函数首先创建了一个叫做time的新的Date对象，因为没有参数，所以time代表了当前日期和时间。然后调用了``getHours`, `getMinutes以及getSeconds方法把当前的时分秒分别赋值给了hour`, `minute`,`second。
```



## 正则表达式

### 创建一个正则表达式

你可以使用以下两种方法构建一个正则表达式：

1. 使用一个**正则表达式字面量**，其由包含在斜杠之间的模式组成，如下所示：

```js
var re = /ab+c/;
```

脚本加载后，正则表达式字面量就会被编译。当正则表达式保持不变时，使用此方法可获得更好的性能。

2. 调用`RegExp`对象的构造函数，如下所示：

```js
var re = new RegExp("ab+c");
```

在脚本运行过程中，用构造函数创建的正则表达式会被编译。如果正则表达式将会改变，或者它将会从用户输入等来源中动态地产生，就需要使用构造函数来创建正则表达式。

| 字符                                                         | 含义                                                         |
| :----------------------------------------------------------- | :----------------------------------------------------------- |
| [`\`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-backslash) | 依照下列规则匹配：在非特殊字符之前的反斜杠表示下一个字符是特殊字符，不能按照字面理解。例如，前面没有 "\" 的 "b" 通常匹配小写字母 "b"，即字符会被作为字面理解，无论它出现在哪里。但如果前面加了 "\"，它将不再匹配任何字符，而是表示一个[字符边界](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#note)。在特殊字符之前的反斜杠表示下一个字符不是特殊字符，应该按照字面理解。详情请参阅下文中的 "转义（Escaping）" 部分。如果你想将字符串传递给 RegExp 构造函数，不要忘记在字符串字面量中反斜杠是转义字符。所以为了在模式中添加一个反斜杠，你需要在字符串字面量中转义它。`/[a-z]\s/i` 和 `new RegExp("[a-z]\\s", "i")` 创建了相同的正则表达式：一个用于搜索后面紧跟着空白字符（`\s` 可看后文）并且在 a-z 范围内的任意字符的表达式。为了通过字符串字面量给 RegExp 构造函数创建包含反斜杠的表达式，你需要在字符串级别和正则表达式级别都对它进行转义。例如 `/[a-z]:\\/i` 和 `new RegExp("[a-z]:\\\\","i")` 会创建相同的表达式，即匹配类似 "C:\" 字符串。 |
| [`^`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-caret) | 匹配输入的开始。如果多行标志被设置为 true，那么也匹配换行符后紧跟的位置。例如，`/^A/` 并不会匹配 "an A" 中的 'A'，但是会匹配 "An E" 中的 'A'。当 '`^`' 作为第一个字符出现在一个字符集合模式时，它将会有不同的含义。[反向字符集合](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-negated-character-set) 一节有详细介绍和示例。 |
| [`$`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-dollar) | 匹配输入的结束。如果多行标志被设置为 true，那么也匹配换行符前的位置。例如，`/t$/` 并不会匹配 "eater" 中的 't'，但是会匹配 "eat" 中的 't'。 |
| [`*`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-asterisk) | 匹配前一个表达式 0 次或多次。等价于 `{0,}`。例如，`/bo*/` 会匹配 "A ghost boooooed" 中的 'booooo' 和 "A bird warbled" 中的 'b'，但是在 "A goat grunted" 中不会匹配任何内容。 |
| [`+`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-plus) | 匹配前面一个表达式 1 次或者多次。等价于 `{1,}`。例如，`/a+/` 会匹配 "candy" 中的 'a' 和 "caaaaaaandy" 中所有的 'a'，但是在 "cndy" 中不会匹配任何内容。 |
| [`?`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-questionmark) | 匹配前面一个表达式 0 次或者 1 次。等价于 `{0,1}`。例如，`/e?le?/` 匹配 "angel" 中的 'el'、"angle" 中的 'le' 以及 "oslo' 中的 'l'。如果**紧跟在任何量词 \*、 +、? 或 {} 的后面**，将会使量词变为**非贪婪**（匹配尽量少的字符），和缺省使用的**贪婪模式**（匹配尽可能多的字符）正好相反。例如，对 "123abc" 使用 `/\d+/` 将会匹配 "123"，而使用 `/\d+?/` 则只会匹配到 "1"。还用于先行断言中，如本表的 `x(?=y)` 和 `x(?!y)` 条目所述。 |
| [`.`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-dot) | （小数点）默认匹配除换行符之外的任何单个字符。例如，`/.n/` 将会匹配 "nay, an apple is on the tree" 中的 'an' 和 'on'，但是不会匹配 'nay'。如果 `s` ("dotAll") 标志位被设为 true，它也会匹配换行符。 |
| [`(x)`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-capturing-parentheses) | 像下面的例子展示的那样，它会匹配 'x' 并且记住匹配项。其中括号被称为*捕获括号*。模式 `/(foo) (bar) \1 \2/` 中的 '`(foo)`' 和 '`(bar)`' 匹配并记住字符串 "foo bar foo bar" 中前两个单词。模式中的 `\1` 和 `\2` 表示第一个和第二个被捕获括号匹配的子字符串，即 `foo` 和 `bar`，匹配了原字符串中的后两个单词。注意 `\1`、`\2`、...、`\n` 是用在正则表达式的匹配环节，详情可以参阅后文的 [\n](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions$edit#special-backreference) 条目。而在正则表达式的替换环节，则要使用像 `$1`、`$2`、...、`$n` 这样的语法，例如，`'bar foo'.replace(/(...) (...)/, '$2 $1')`。`$&` 表示整个用于匹配的原字符串。 |
| [`(?:x)`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-non-capturing-parentheses) | 匹配 'x' 但是不记住匹配项。这种括号叫作*非捕获括号*，使得你能够定义与正则表达式运算符一起使用的子表达式。看看这个例子 `/(?:foo){1,2}/`。如果表达式是 `/foo{1,2}/`，`{1,2}` 将只应用于 'foo' 的最后一个字符 'o'。如果使用非捕获括号，则 `{1,2}` 会应用于整个 'foo' 单词。更多信息，可以参阅下文的 [Using parentheses](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#Using_parentheses) 条目. |
| [`x(?=y)`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-lookahead) | 匹配'x'仅仅当'x'后面跟着'y'.这种叫做先行断言。例如，/Jack(?=Sprat)/会匹配到'Jack'仅当它后面跟着'Sprat'。/Jack(?=Sprat\|Frost)/匹配‘Jack’仅当它后面跟着'Sprat'或者是‘Frost’。但是‘Sprat’和‘Frost’都不是匹配结果的一部分。 |
| [`(?<=y)`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-lookahead)x | 匹配'x'仅当'x'前面是'y'.这种叫做后行断言。例如，/(?<=Jack)Sprat/会匹配到' Sprat '仅仅当它前面是' Jack '。/(?<=Jack\|Tom)Sprat/匹配‘ Sprat ’仅仅当它前面是'Jack'或者是‘Tom’。但是‘Jack’和‘Tom’都不是匹配结果的一部分。 |
| [`x(?!y)`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-negated-look-ahead) | 仅仅当'x'后面不跟着'y'时匹配'x'，这被称为正向否定查找。例如，仅仅当这个数字后面没有跟小数点的时候，/\d+(?!\.)/ 匹配一个数字。正则表达式/\d+(?!\.)/.exec("3.141")匹配‘141’而不是‘3.141’ |
| `(?<!*y*)*x*`                                                | 仅仅当'x'前面不是'y'时匹配'x'，这被称为反向否定查找。例如, 仅仅当这个数字前面没有负号的时候，`/(?<!-)\d+/` 匹配一个数字。 `/(?<!-)\d+/.exec('3')` 匹配到 "3". `/(?<!-)\d+/.exec('-3')` 因为这个数字前有负号，所以没有匹配到。 |
| [`x|y`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-or) | 匹配‘x’或者‘y’。例如，/green\|red/匹配“green apple”中的‘green’和“red apple”中的‘red’ |
| [`{n}`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-quantifier) | n 是一个正整数，匹配了前面一个字符刚好出现了 n 次。 比如， /a{2}/ 不会匹配“candy”中的'a',但是会匹配“caandy”中所有的 a，以及“caaandy”中的前两个'a'。 |
| [`{n,}`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-quantifier) | n是一个正整数，匹配前一个字符至少出现了n次。例如, /a{2,}/ 匹配 "aa", "aaaa" 和 "aaaaa" 但是不匹配 "a"。 |
| [`{n,m}`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-quantifier-range) | n 和 m 都是整数。匹配前面的字符至少n次，最多m次。如果 n 或者 m 的值是0， 这个值被忽略。例如，/a{1, 3}/ 并不匹配“cndy”中的任意字符，匹配“candy”中的a，匹配“caandy”中的前两个a，也匹配“caaaaaaandy”中的前三个a。注意，当匹配”caaaaaaandy“时，匹配的值是“aaa”，即使原始的字符串中有更多的a。 |
| `[xyz]`                                                      | 一个字符集合。匹配方括号中的任意字符，包括[转义序列](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Grammar_and_types)。你可以使用破折号（-）来指定一个字符范围。对于点（.）和星号（*）这样的特殊符号在一个字符集中没有特殊的意义。他们不必进行转义，不过转义也是起作用的。 例如，[abcd] 和[a-d]是一样的。他们都匹配"brisket"中的‘b’,也都匹配“city”中的‘c’。/[a-z.]+/ 和/[\w.]+/与字符串“test.i.ng”匹配。 |
| `[^xyz]`                                                     | 一个反向字符集。也就是说， 它匹配任何没有包含在方括号中的字符。你可以使用破折号（-）来指定一个字符范围。任何普通字符在这里都是起作用的。例如，[^abc] 和 [^a-c] 是一样的。他们匹配"brisket"中的‘r’，也匹配“chop”中的‘h’。 |
| `[\b]`                                                       | 匹配一个退格(U+0008)。（不要和\b混淆了。）                   |
| [`\b`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-word-boundary) | 匹配一个词的边界。一个词的边界就是一个词不被另外一个“字”字符跟随的位置或者前面跟其他“字”字符的位置，例如在字母和空格之间。注意，匹配中不包括匹配的字边界。换句话说，一个匹配的词的边界的内容的长度是0。（不要和[\b]混淆了）使用"moon"举例： /\bm/匹配“moon”中的‘m’； /oo\b/并不匹配"moon"中的'oo'，因为'oo'被一个“字”字符'n'紧跟着。 /oon\b/匹配"moon"中的'oon'，因为'oon'是这个字符串的结束部分。这样他没有被一个“字”字符紧跟着。 /\w\b\w/将不能匹配任何字符串，因为在一个单词中间的字符永远也不可能同时满足没有“字”字符跟随和有“字”字符跟随两种情况。**注意:** JavaScript的正则表达式引擎将[特定的字符集](http://www.ecma-international.org/ecma-262/5.1/#sec-15.10.2.6)定义为“字”字符。不在该集合中的任何字符都被认为是一个断词。这组字符相当有限：它只包括大写和小写的罗马字母，十进制数字和下划线字符。不幸的是，重要的字符，例如“é”或“ü”，被视为断词。 |
| [`\B`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-non-word-boundary) | 匹配一个非单词边界。匹配如下几种情况：字符串第一个字符为非“字”字符字符串最后一个字符为非“字”字符两个单词字符之间两个非单词字符之间空字符串例如，/\B../匹配"noonday"中的'oo', 而/y\B../匹配"possibly yesterday"中的’yes‘ |
| [`\c*X*`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-control) | 当X是处于A到Z之间的字符的时候，匹配字符串中的一个控制符。例如，`/\cM/` 匹配字符串中的 control-M (U+000D)。 |
| [`\d`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-digit) | 匹配一个数字`。``等价于[0-9]`。例如， `/\d/` 或者 `/[0-9]/` 匹配"B2 is the suite number."中的'2'。 |
| [`\D`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-non-digit) | 匹配一个非数字字符`。``等价于[^0-9]`。例如， `/\D/` 或者 `/[^0-9]/` 匹配"B2 is the suite number."中的'B' 。 |
| [`\f`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-form-feed) | 匹配一个换页符 (U+000C)。                                    |
| [`\n`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-line-feed) | 匹配一个换行符 (U+000A)。                                    |
| [`\r`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-carriage-return) | 匹配一个回车符 (U+000D)。                                    |
| [`\s`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-white-space) | 匹配一个空白字符，包括空格、制表符、换页符和换行符。等价于[ \f\n\r\t\v\u00a0\u1680\u180e\u2000-\u200a\u2028\u2029\u202f\u205f\u3000\ufeff]。例如, `/\s\w*/` 匹配"foo bar."中的' bar'。经测试，\s不匹配"[\u180e](https://unicode-table.com/cn/180E/)"，在当前版本Chrome(v80.0.3987.122)和Firefox(76.0.1)控制台输入/\s/.test("\u180e")均返回false。 |
| [`\S`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-non-white-space) | 匹配一个非空白字符。等价于 `[^ `\f\n\r\t\v\u00a0\u1680\u180e\u2000-\u200a\u2028\u2029\u202f\u205f\u3000\ufeff`]`。例如，`/\S\w*/` 匹配"foo bar."中的'foo'。 |
| [`\t`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-tab) | 匹配一个水平制表符 (U+0009)。                                |
| [`\v`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-vertical-tab) | 匹配一个垂直制表符 (U+000B)。                                |
| [`\w`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-word) | 匹配一个单字字符（字母、数字或者下划线）。等价于 `[A-Za-z0-9_]`。例如, `/\w/` 匹配 "apple," 中的 'a'，"$5.28,"中的 '5' 和 "3D." 中的 '3'。 |
| [`\W`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-non-word) | 匹配一个非单字字符。等价于 `[^A-Za-z0-9_]`。例如, `/\W/` 或者 `/[^A-Za-z0-9_]/` 匹配 "50%." 中的 '%'。 |
| [`\*n*`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-backreference) | 在正则表达式中，它返回最后的第n个子捕获匹配的子字符串(捕获的数目以左括号计数)。比如 `/apple(,)\sorange\1/` 匹配"apple, orange, cherry, peach."中的'apple, orange,' 。 |
| [`\0`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-null) | 匹配 NULL（U+0000）字符， 不要在这后面跟其它小数，因为 `\0<digits>` 是一个八进制转义序列。 |
| [`\xhh`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-hex-escape) | 匹配一个两位十六进制数（\x00-\xFF）表示的字符。              |
| [`\uhhhh`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#special-unicode-escape) | 匹配一个四位十六进制数表示的 UTF-16 代码单元。               |
| `\u{hhhh}或\u{hhhhh}`                                        | （仅当设置了u标志时）匹配一个十六进制数表示的 Unicode 字符。 |

## 使用正则表达式

正则表达式可以被用于 `RegExp` 的 [`exec`](https://developer.mozilla.org/zh-CN/docs/JavaScript/Reference/Global_Objects/RegExp/exec) 和 [`test`](https://developer.mozilla.org/zh-CN/docs/JavaScript/Reference/Global_Objects/RegExp/test) 方法以及 [`String`](https://developer.mozilla.org/zh-CN/docs/JavaScript/Reference/Global_Objects/String) 的 [`match`](https://developer.mozilla.org/zh-CN/docs/JavaScript/Reference/Global_Objects/String/match)、[`replace`](https://developer.mozilla.org/zh-CN/docs/JavaScript/Reference/Global_Objects/String/replace)、[`search`](https://developer.mozilla.org/zh-CN/docs/JavaScript/Reference/Global_Objects/String/search) 和 [`split`](https://developer.mozilla.org/zh-CN/docs/JavaScript/Reference/Global_Objects/String/split) 方法。这些方法在 [JavaScript 手册](https://developer.mozilla.org/zh-CN/docs/JavaScript/Reference)中有详细的解释。

| **方法**                                                     | **描述**                                                     |
| :----------------------------------------------------------- | :----------------------------------------------------------- |
| **[`exec`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/RegExp/exec)** | **一个在字符串中执行查找匹配的RegExp方法，它返回一个数组（未匹配到则返回 null）。** |
| **[`test`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/RegExp/test)** | **一个在字符串中测试是否匹配的RegExp方法，它返回 true 或 false。** |
| **[`match`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/match)** | 一个**在字符串中**执行**查找匹配的String方法**，它返回一个数组，在未匹配到时会返回 null。 |
| [`matchAll`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/matchAll) | 一个在字符串中执行查找所有匹配的String方法，它返回一个迭代器（iterator）。 |
| [`search`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/search) | 一个在字符串中测试匹配的String方法，它返回匹配到的位置索引，或者在失败时返回-1。 |
| [`replace`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/replace) | 一个在字符串中执行查找匹配的String方法，并且使用替换字符串替换掉匹配到的子字符串。 |
| [`split`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/split) | 一个使用正则表达式或者一个固定字符串分隔一个字符串，并将分隔后的子字符串存储到数组中的 `String` 方法。 |

**当你想要知道在一个字符串中的一个匹配是否被找到，你可以使用 test 或 search 方法；想得到更多的信息（但是比较慢）则可以使用 exec 或 match 方法**。如果你使用exec 或 match 方法并且匹配成功了，那么这些方法将返回一个数组并且更新相关的正则表达式对象的属性和预定义的正则表达式对象（详见下）。如果匹配失败，那么 exec 方法返回 null（也就是false）。



| 对象      | 属性或索引                                     | 描述                                                         | 在例子中对应的值 |
| :-------- | :--------------------------------------------- | :----------------------------------------------------------- | :--------------- |
| `myArray` |                                                | 匹配到的字符串和所有被记住的子字符串。                       | `["dbbd", "bb"]` |
| `index`   | 在输入的字符串中匹配到的以0开始的索引值。      | `1`                                                          |                  |
| `input`   | 初始字符串。                                   | `"cdbbdbsbz"`                                                |                  |
| `[0]`     | 最近一个匹配到的字符串。                       | `"dbbd"`                                                     |                  |
| `myRe`    | `lastIndex`                                    | 开始下一个匹配的起始索引值。（这个属性只有在使用g参数时可用在 [通过参数进行高级搜索](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Regular_Expressions#.E9.80.9A.E8.BF.87.E5.8F.82.E6.95.B0.E8.BF.9B.E8.A1.8C.E9.AB.98.E7.BA.A7.E6.90.9C.E7.B4.A2) 一节有详细的描述.) | `5`              |
| `source`  | 模式字面文本。在正则表达式创建时更新，不执行。 | `"d(b+)d"`                                                   |                  |

- **使用括号的子字符串匹配**

一个正则表达式模式使用括号，将导致相应的子匹配被记住。例如，/a(b)c /可以匹配字符串“abc”，并且记得“b”。回调这些括号中匹配的子串，使用数组元素[1],……[n]。

使用括号匹配的子字符串的数量是无限的。返回的数组中保存所有被发现的子匹配。下面的例子说明了如何使用括号的子字符串匹配。

下面的脚本使用replace()方法来转换字符串中的单词。在匹配到的替换文本中，脚本使用替代的$ 1,$ 2表示第一个和第二个括号的子字符串匹配。

```js
var re = /(\w+)\s(\w+)/;
var str = "John Smith";
var newstr = str.replace(re, "$2, $1");
console.log(newstr);
```

这个表达式输出 "Smith, John"。

### 通过标志进行高级搜索



正则表达式有六个可选参数 (`flags`) 允许全局和不分大小写搜索等。这些参数既可以单独使用也能以任意顺序一起使用, 并且被包含在正则表达式实例中。

| 标志 | 描述                                                      |
| :--- | :-------------------------------------------------------- |
| `g`  | 全局搜索。                                                |
| `i`  | 不区分大小写搜索。                                        |
| `m`  | 多行搜索。                                                |
| `s`  | 允许 `.` 匹配换行符。                                     |
| `u`  | 使用unicode码的模式进行匹配。                             |
| `y`  | 执行“粘性(`sticky`)”搜索,匹配从目标字符串的当前位置开始。 |

为了在正则表达式中包含标志，请使用以下语法：

```js
var re = /pattern/flags;
```

或者

```js
var re = new RegExp("pattern", "flags");
```



## 字符串

| 方法                                                         | 描述                                                         |
| :----------------------------------------------------------- | :----------------------------------------------------------- |
| [`charAt`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/charAt), [`charCodeAt`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/charCodeAt), [`codePointAt`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/codePointAt) | 返回字符串指定位置的字符或者字符编码。                       |
| [`indexOf`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/indexOf), [`lastIndexOf`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/lastIndexOf) | 分别返回字符串中指定子串的位置或最后位置。                   |
| [`startsWith`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/startsWith), [`endsWith`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/endsWith), [`includes`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/includes) | 返回字符串是否以指定字符串开始、结束或包含指定字符串。       |
| [`concat`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/concat) | 连接两个字符串并返回新的字符串。                             |
| [`fromCharCode`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/fromCharCode), [`fromCodePoint`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/fromCodePoint) | 从指定的Unicode值序列构造一个字符串。这是一个String类方法，不是实例方法。 |
| [`split`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/split) | 通过将字符串分离成一个个子串来把一个String对象分裂到一个字符串数组中。 |
| [`slice`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/slice) | 从一个字符串提取片段并作为新字符串返回。                     |
| [`substring`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/substring), [`substr`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/substr) | 分别通过指定起始和结束位置，起始位置和长度来返回字符串的指定子集。 |
| [`match`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/match), [`replace`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/replace), [`search`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/search) | 通过正则表达式来工作.                                        |
| [`toLowerCase`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/toLowerCase), [`toUpperCase`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/toUpperCase) | 分别返回字符串的小写表示和大写表示。                         |
| [`normalize`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/normalize) | 按照指定的一种 Unicode 正规形式将当前字符串正规化。          |
| [`repeat`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/repeat) | 将字符串内容重复指定次数后返回。                             |
| [`trim`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String/trim) | 去掉字符串开头和结尾的空白字符。                             |

##  promise笔记 ---author:前端静径

### 前言

一直想写一篇关于promise的文来总结一下之前零零散散的promise知识点，趁着工作闲暇，来做个总结。PS:本文适合有一定`JavaScript`基础的童鞋阅读。

### 什么是promise

如果说到JavaScript的异步处理，我想大多数人都会想到利用回调函数：

```
//示例：使用了回调函数的异步处理
http.get('/v1/get', function(error, data) {
    if (error) {
        //错误时的处理
    }
    //成功时的处理
})
```

像上面这样基于回调函数的异步处理如果统一参数使用规则的话，写法也会很明了。但是，这也仅是编码规约而已，即使采用不同的写法也不会报错。

而`Promise`则是把类似的异步处理对象和处理规则进行规范化，并按照采用统一的接口来编写，而采取规定方法之外的写法都会报错。下面看一个例子：

```
var promise = http.get('/v1/get');
promise.then(function(result) {
    //成功时的处理
}).catch(function(error) {
    //错误时的处理
})
```

可以看到，这里在使用promise进行一步处理的时候，我们必须按照接口规定的方法编写处理代码。也就是说，除promise对象规定的方法(这里的 `then` 或 `catch`)以外的方法都是不可以使用的， 而不会像回调函数方式那样可以自己自由的定义回调函数的参数，而必须严格遵守固定、统一的编程方式来编写代码。这样，基于Promise的统一接口的做法， 就可以形成基于接口的各种各样的异步处理模式。

但这并不是使用`promise`的足够理由，`promise`为异步操作提供了统一的接口，能让代码不至于陷入回调嵌套的死路中，它的强大之处在于它的链式调用（文章后面会有提及）。

### 基本用法

promise的语法：

```
new Promise(function(resolve, reject) {
    //待处理的异步逻辑
    //处理结束后，调用resolve或reject方法
})
```



新建一个`promise`很简单，只需要`new`一个`promise`对象即可。所以`promise`本质上就是一个函数，它接受一个函数作为参数，并且会返回`promise对象`，这就给链式调用提供了基础。

其实`Promise`函数的使命，就是构建出它的实例，并且负责帮我们管理这些实例。而这些实例有以下三种状态：

1. pending: 初始状态，位履行或拒绝
2. fulfilled: 意味着操作成功完成
3. rejected: 意味着操作失败

`pending` 状态的 `Promise对象`可能以 `fulfilled`状态返回了一个值，也可能被某种理由（异常信息）拒绝（`reject`）了。当其中任一种情况出现时，`Promise` 对象的 `then` 方法绑定的处理方法（`handlers`）就会被调用，`then`方法分别指定了`resolve`方法和`reject`方法的回调函数

一图胜千言：

[![promise](/vuepress_blog/img/promises.png)

简单的示例:

```
var promise = new Promise(function(resolve, reject) {
  if (/* 异步操作成功 */){
    resolve(value);
  } else {
    reject(error);
  }
});

promise.then(function(value) {
  // 如果调用了resolve方法，执行此函数
}, function(value) {
  // 如果调用了reject方法，执行此函数
});
```



上述代码很清晰的展示了promise对象运行的机制。下面再看一个示例：

```
var getJSON = function(url) {
  var promise = new Promise(function(resolve, reject){
    var client = new XMLHttpRequest();
    client.open("GET", url);
    client.onreadystatechange = handler;
    client.responseType = "json";
    client.setRequestHeader("Accept", "application/json");
    client.send();

    function handler() {
      if (this.status === 200) { 
              resolve(this.response); 
          } else { 
              reject(new Error(this.statusText)); 
          }
    };
  });

  return promise;
};

getJSON("/posts.json").then(function(json) {
  console.log('Contents: ' + json);
}, function(error) {
  console.error('出错了', error);
});
```



上面代码中，resolve方法和reject方法调用时，都带有参数。它们的参数会被传递给回调函数。reject方法的参数通常是Error对象的实例，而resolve方法的参数除了正常的值以外，还可能是另一个Promise实例，比如像下面这样。

```
var p1 = new Promise(function(resolve, reject){
  // ... some code
});

var p2 = new Promise(function(resolve, reject){
  // ... some code
  resolve(p1);
})
```

上面代码中，p1和p2都是Promise的实例，但是p2的resolve方法将p1作为参数，这时p1的状态就会传递给p2。如果调用的时候，p1的状态是pending，那么p2的回调函数就会等待p1的状态改变；如果p1的状态已经是fulfilled或者rejected，那么p2的回调函数将会立刻执行。

### promise的链式操作

正如前面提到的，Promise.prototype.then方法返回的是一个新的Promise对象，因此可以采用链式写法。

```
getJSON("/visa.json").then(function(json) {
  return json.name;
}).then(function(name) {
  // proceed
});
```

上面的代码使用then方法，依次指定了两个回调函数。第一个回调函数完成以后，会将返回结果作为参数，传入第二个回调函数。

如果前一个回调函数返回的是Promise对象，这时后一个回调函数就会等待该Promise对象有了运行结果，才会进一步调用。

```
getJSON("/visa/get.json").then(function(post) {
  return getJSON(post.jobURL);
}).then(function(jobs) {
  // 对jobs进行处理
});
```

这种设计使得嵌套的异步操作，可以被很容易得改写，从回调函数的“横向发展”改为“向下发展”。

### promise捕获错误

Promise.prototype.catch方法是Promise.prototype.then(null, rejection)的别名，用于指定发生错误时的回调函数。

```
getJSON("/visa.json").then(function(result) {
  // some code
}).catch(function(error) {
  // 处理前一个回调函数运行时发生的错误
  console.log('出错啦！', error);
});
```

Promise对象的错误具有“冒泡”性质，会一直向后传递，直到被捕获为止。也就是说，错误总是会被下一个catch语句捕获。

```
getJSON("/visa.json").then(function(json) {
  return json.name;
}).then(function(name) {
  // proceed
}).catch(function(error) {
    //处理前面任一个then函数抛出的错误
});
```

### 其他常用的promise方法

### Promise.all方法，Promise.race方法

Promise.all方法用于将多个Promise实例，包装成一个新的Promise实例。

```
var p = Promise.all([p1,p2,p3]);
```

上面代码中，Promise.all方法接受一个数组作为参数，p1、p2、p3都是Promise对象的实例。（Promise.all方法的参数不一定是数组，但是必须具有iterator接口，且返回的每个成员都是Promise实例。）

p的状态由p1、p2、p3决定，分成两种情况。

1. 只有p1、p2、p3的状态都变成fulfilled，p的状态才会变成fulfilled，此时p1、p2、p3的返回值组成一个数组，传递给p的回调函数。
2. 只要p1、p2、p3之中有一个被rejected，p的状态就变成rejected，此时第一个被reject的实例的返回值，会传递给p的回调函数。

下面是一个具体的例子。

```
// 生成一个Promise对象的数组
var promises = [2, 3, 5, 7, 11, 13].map(function(id){
  return getJSON("/get/addr" + id + ".json");
});

Promise.all(promises).then(function(posts) {
  // ...  
}).catch(function(reason){
  // ...
});
```

Promise.race方法同样是将多个Promise实例，包装成一个新的Promise实例。

```
var p = Promise.race([p1,p2,p3]);
```

上面代码中，只要p1、p2、p3之中有一个实例率先改变状态，p的状态就跟着改变。那个率先改变的Promise实例的返回值，就传递给p的返回值。

如果Promise.all方法和Promise.race方法的参数，不是Promise实例，就会先调用下面讲到的Promise.resolve方法，将参数转为Promise实例，再进一步处理。

### Promise.resolve方法，Promise.reject方法

有时需要将现有对象转为Promise对象，Promise.resolve方法就起到这个作用。

```
var jsPromise = Promise.resolve($.ajax('/whatever.json'));
```

上面代码将jQuery生成deferred对象，转为一个新的ES6的Promise对象。

如果Promise.resolve方法的参数，不是具有then方法的对象（又称thenable对象），则返回一个新的Promise对象，且它的状态为fulfilled。

```
var p = Promise.resolve('Hello');

p.then(function (s){
  console.log(s)
});
// Hello
```

上面代码生成一个新的Promise对象的实例p，它的状态为fulfilled，所以回调函数会立即执行，Promise.resolve方法的参数就是回调函数的参数。

如果Promise.resolve方法的参数是一个Promise对象的实例，则会被原封不动地返回。

Promise.reject(reason)方法也会返回一个新的Promise实例，该实例的状态为rejected。Promise.reject方法的参数reason，会被传递给实例的回调函数。

```
var p = Promise.reject('出错啦');

p.then(null, function (error){
  console.log(error)
});
// 出错了
```

上面代码生成一个Promise对象的实例p，状态为rejected，回调函数会立即执行。

### 题外话：async函数

async函数是es7提案出来的语法，并不属于es6，但是已经有一些平台和编辑器支持这种函数了，所以这里也做一下了解。

async函数是用来取代回调函数的另一种方法。

只要函数名之前加上async关键字，就表明该函数内部有异步操作。该异步操作应该返回一个Promise对象，前面用await关键字注明。当函数执行的时候，一旦遇到await就会先返回，等到触发的异步操作完成，再接着执行函数体内后面的语句。

```
async function getStockPrice(symbol, currency) {
    let price = await getStockPrice(symbol);
    return convert(price, currency);
}
```

上面代码是一个获取股票报价的函数，函数前面的async关键字，表明该函数将返回一个Promise对象。调用该函数时，当遇到await关键字，立即返回它后面的表达式（getStockPrice函数）产生的Promise对象，不再执行函数体内后面的语句。等到getStockPrice完成，再自动回到函数体内，执行剩下的语句。

下面是一个更一般性的例子。

```
function timeout(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

async function asyncValue(value) {
  await timeout(50);
  return value;
}
```

上面代码中，asyncValue函数前面有async关键字，表明函数体内有异步操作。执行的时候，遇到await语句就会先返回，等到timeout函数执行完毕，再返回value。

个人觉得async函数将异步发挥到了极致，代码看上去更加简洁，更加舒服了，而且其流程也很好理解。

### 总结

promise作为异步操作的规则，确实给开发带来了不少便利，至少不用像回调那样，出现函数里面套函数这种无限的嵌套的情况，promise让你的代码变得更加的优雅了。当然如果之后async函数变得更加普及，那么就更好了。

下面来看看下面这道题目，大家可以思考下结果是多少？

```
console.log(1);
new Promise(function (resolve, reject){
    reject(true);
    window.setTimeout(function (){
        resolve(false);
    }, 0);
}).then(function(){
    console.log(2);
}, function(){
    console.log(3);
});
console.log(4);
```

答案我就不贴出来了，给大家思考的空间，实在不知道结果的也可以直接复制这段代码到浏览器控制台执行下，也能很快的出结果。

## 数组去重复的方法

1.利用ES6+Array.from去重

2.利用for嵌套for，然后splice去重，判断第一项与后面所有项

3.利用indexOf去重

4.利用sort（），排序后（unicode编码排序）前一项与后一项比较

5.利用includes 和indexof差不多

6.利用filter+indexOf判断当前值索引是不是index

7.利用reduce+includes去重

------

## 深想科技面试题



### 1.Array和Array.of相同点和区别。类数组怎么转变成普通数组？

* Array()是构造方法。
  `a = new Array(1, 2, 3);`
  `a = Array(1, 2, 3);` // `new`可省略

* `Array.of()`是静态方法，也返回一个数组。
  `Array.of(...elements)` 创建一个具有可变数量参数的新的数组实例。

**功能区别**

* 如果只传入一个参数，且这个参数为`Number`数值类型。
  `new Array(3)` 返回`(4) [empty × 4]`，有3个空位`empty`的数组，长度为3。
  `Array.of(3)` 依旧把`唯一的数值参数`作为元素，返回一个新数组，长度为1。

`Array.of(...elements)`是ES6标准的一部分，如果浏览器不支持，可以提前执行以下代码，自行创建`Array.of()`方法

**类数组怎么转变成普通数组**？

* **slice**

  - ##### Array.prototype.slice.call(arguments)

  - ##### [].slice.call(arguments);

* **Array.from()**
  * 从一个类似数组或可迭代对象创建一个新的，浅拷贝的数组实例。
  * **`Array.from(obj, mapFn, thisArg) 就相当于 Array.from(obj).map(mapFn, thisArg)`**
* **`[...xxx]` 扩展运算符**
  * ES6中的扩展运算符`...`也能将某些数据结构转换成数组，这种数据结构必须有遍历器接口
  * var args = [...arguments];
* **jquery的$.makeArray()**
  
  * var arr = $.makeArray(arguments);

### 2.promise的执行流程，promise如何实现链式调用

[`Promise`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Promise) 是一个对象，它代表了一个异步操作的最终完成或者失败

Promise 必须为以下三种状态之一：等待态（Pending）、执行态（Fulfilled）和拒绝态（Rejected）。一旦Promise 被 resolve 或 reject，不能再迁移至其他任何状态（即状态 immutable）。

基本过程：

1. 初始化 Promise 状态（pending）
2. 立即执行 Promise 中传入的 fn 函数，将Promise 内部 resolve、reject 函数作为参数传递给 fn ，按事件机制时机处理
3. 执行 then(..) 注册回调处理数组（then 方法可被同一个 promise 调用多次）
4. Promise里的关键是要保证，then方法传入的参数 onFulfilled 和 onRejected，必须在then方法被调用的那一轮事件循环之后的新执行栈中执行。

> ####  **补充：回调函数与普通函数的区别**
>
> 1. 对普通函数的调用：调用程du序发出对普通函数的调用后zhi，程序执行立即转向被调用函数执行，直到被调用函数执行完毕后，再返回调用程序继续执行。从发出调用的程序的角度看，这个过程为**“调用-->等待被调用函数执行完毕-->继续执行**”。
>
>  	2. 对回调函数调用：**调用程序发出对回调函数的调用后，不等函数执行完毕，立即返回并继续执行。这样，调用程序执和被调用函数同时在执行。当被调函数执行完毕后，被调函数会反过来调用某个事先指定函数，以通知调用程序：函数调用结束。这个过程称为回调（Callback）**，这正是回调函数名称的由来。

而`Promise`则是把类似的异步处理对象和处理规则进行规范化，并按照采用统一的接口来编写，而采取规定方法之外的写法都会报错。

new Promise((resolve,reject)).then().then()就能实现链式调用，原理是then函数里面返回了新的Promise对象，而then是原型上的方法，自然新的promise对象上也有then方法。

重点：

- `Promise`则是把类似的异步处理对象和处理规则进行规范化，并按照采用统一的接口来编写，而采取规定方法之外的写法都会报错。

- Promise对象的错误具有“冒泡”性质，会一直向后传递，直到被捕获为止。也就是说，错误总是会被下一个catch语句捕获

- 上面的代码使用then方法，依次指定了两个回调函数。第一个回调函数完成以后，会将返回结果作为参数，传入第二个回调函数。

- 如果前一个回调函数返回的是Promise对象，这时后一个回调函数就会等待该Promise对象有了运行结果，才会进一步调用。

### 3.Generator和async的区别

1. Generator函数是ES6提供的一种异步解决方案，它可以理解为一个状态机，封装了多个内部状态。执行Generator函数会返回一个遍历器对象。所以Generator还是一个遍历器生成函数。

写法：function 关键字与函数名之间有一个星号，函数体内使用yield表达式，定义不同的内部状态。

调用Generator函数，该函数不会执行，返回一个指向内部状态的指针对象。调用next方法使指针移动到下一个状态。

总结一下，调用 Generator 函数，返回一个遍历器对象，代表 Generator 函数的内部指针。以后，每次调用遍历器对象的`next`方法，就会返回一个有着`value`和`done`两个属性的对象。`value`属性表示当前的内部状态的值，是`yield`表达式后面那个表达式的值；`done`属性是一个布尔值，表示是否遍历结束。

2.ES2017 标准引入了 async 函数，使得异步操作变得更加方便。

async 函数，它就是 Generator 函数的语法糖。

写法：`async`函数就是将 Generator 函数的星号（`*`）替换成`async`，将`yield`替换成`await`，仅此而已。

`async`函数对 Generator 函数的改进：

（1）内置执行器。（2）更好的语义。（3）更广的适用性。`await`命令后面，可以是 Promise 对象和原始类型的值（数值、字符串和布尔值，但这时会自动转成立即 resolved 的 Promise 对象）（4）返回值是 Promise。

**Promise 对象的状态变化**

`async`函数返回的 Promise 对象，必须等到内部所有`await`命令后面的 Promise 对象执行完，才会发生状态改变，除非遇到`return`语句或者抛出错误。也就是说，只有`async`函数内部的异步操作执行完，才会执行`then`方法指定的回调函数。

### **4.如何在js的for循环中，顺序执行异步请求**

```js
const family = {
  "dad": 150,
  "mom": 100,
  "son": 200
}
const familyToGet = ["dad", "mom", "son"]


// 写一个sleep方法
const sleep = ms => {
  return new Promise(resolve => setTimeout(resolve, ms))
}
// 假设获取体重数据的过程是一个请求接口的异步过程
const getFamilyWeight = person => {
  return sleep(2000).then(() => family[person])
}

const loop_for = async () => {
  console.log('start')
  let result = 0
  for(let i = 0; i < familyToGet.length; i++) {
    const person = familyToGet[i]
    const weight = await getFamilyWeight(person)
    result += weight
  }
  console.log('result:', result)
  console.log('end')
}

loop_for()
// 运行结果
// start
// 150
// 100
// 200
// result: 450
// end
```

### 5.实现数组的随机排序



```js
 //随机排序
function sort(arr){
    let result = [];
    while(arr.length>0){
        var len = arr.length;
        var index = Math.floor(Math.random()*len);
        result.push(arr.splice(index,1)[0]);
    }
    return result;
}
var arrs = [1,2,3,4,5,6,7,8];
console.log(sort(arrs));
```

### 6.实现讲多维数组扁平化，即转为一维数组

```js
// 1. 递归
function flatten(arr){
    let result = [];
    for(let i =0;i<arr.length;i++){
        if(arr[i] instanceof Array){
            result = result.concat(flatten(arr[i]))
         }
        else{
            result.push(arr[i]);
        }
    }
    return result;
}
var arrs = [[[[1,3],[1,3,4]],[[1,3],[1,3,4]]],[[[1,3],[1,3,4]],[[1,3],[1,3,4]]]];
console.log(flatten(arrs));
// 2. toString();
function flatten1(arr){
    return arr.toString().split(",").map(item=>{
        return +item;
    })
}
console.log(flatten1(arrs));


//3. reduce
function flatten2(arr){
    return arr.reduce(function(prev,next){
        return prev.concat(Array.isArray(next)?flatten2(next):next)
    },[]);
}
console.log(flatten2(arrs));

//4 rest运算符  注意, 由于while里面改变了arr的值，所以执行结果是一层层播壳。向递归
function flatten3(arr){
    while(arr.some(item=>Array.isArray(item))){
        console.log("=");
        arr = [].concat(...arr); // 每次只能解开一层数组
        console.log(arr);
    }
    return arr;
}
console.log(flatten3(arrs));
```

### 7.栈的实现，pop，push，显示栈顶元素，显示栈的长度，清空栈。（不能使用Array.prototype方法）

```js
function Stack(){
    this.container = [];
}
Stack.prototype.push=function(arg){
    this.container[this.container.length]=arg;
} 
Stack.prototype.pop=function(arg){
    return this.container.splice(this.container.length-1,1)[0];
}
Stack.prototype.showTop=function(){
    return this.container[this.container.length-1];
}
Stack.prototype.showLen = function(){
    return this.container.length;
}
Stack.prototype.clear=function(){
    this.container.length=0;
}
```

### 8.从 URL 输入到页面展现到底发生什么？

- **浏览器缓存**
- **DNS域名解析**
- **建立Tcp连接**
- **浏览器向web服务器发送一个http请求**
- **发送响应数据给客户端**
- **浏览器解析渲染页面**
  - 浏览器解析渲染页面分为一下五个步骤：
    - 根据 HTML 解析出 DOM 树
    - 根据 CSS 解析生成 CSS 规则树
    - 结合 DOM 树和 CSS 规则树，生成渲染树
    - 根据渲染树计算每一个节点的信息
    - 根据计算好的信息绘制页面

> 补充：浏览器渲染机制

![浏览器渲染机制](/vuepress_blog/img/render.png)

### 9. 闭包是什么，闭包作用场景，闭包风险？

闭包是什么？

>  闭包是指在 JavaScript 中，内部函数总是可以访问其所在的外部函数中声明的参数和变量，即使在其外部函数被返回（寿命终结）了之后.

> 闭包是 JavaScript 一个非常重要的特性，这意味着当前作用域总是能够访问外部作用域中的变量。 因为 函数 是 JavaScript 中唯一拥有自身作用域的结构，因此闭包的创建依赖于函数。

闭包的应用场景

1. **闭包应用场景之setTimeout**

    setTimeout(function(param){
        alert(param)
    },1000)
    
    //通过闭包可以实现传参效果
    function func(param){
        return function(){
            alert(param)
        }
    }
    var f1 = func(1);
    setTimeout(f1,1000);
2. **闭包应用场景之回调**

**我们定义行为，然后把它关联到某个用户事件上（点击或者按键）。我们的代码通常会作为一个回调（事件触发时调用的函数）绑定到事件上**

```js
<body>
    <p>哈哈哈哈哈哈</p>
    <h1>hhhhhhhhh</h1>
    <h2>qqqqqqqqq</h2>
    <a href="#" id="size-12">12</a>
    <a href="#" id="size-14">14</a>
    <a href="#" id="size-16">16</a>

<script>
    function changeSize(size){
        return function(){
            document.body.style.fontSize = size + 'px';
        };
    }

    var size12 = changeSize(12);
    var size14 = changeSize(14);
    var size16 = changeSize(16);

    document.getElementById('size-12').onclick = size12;
    document.getElementById('size-14').onclick = size14;
    document.getElementById('size-16').onclick = size16;
</script>
</body>

```

3. **闭包应用场景之封装变量**

**用闭包定义能访问私有函数和私有变量的公有函数**

```
    var counter = (function(){
        var privateCounter = 0; //私有变量
        function change(val){
            privateCounter += val;
        }
        return {
            increment:function(){   //三个闭包共享一个词法环境
                change(1);
            },
            decrement:function(){
                change(-1);
            },
            value:function(){
                return privateCounter;
            }
        };
    })();

    console.log(counter.value());//0
    counter.increment();
    counter.increment();//2
复制代码
```

1. 共享的环境创建在一个匿名函数体内，立即执行。
2. 环境中有一个局部变量一个局部函数，通过匿名函数返回的对象的三个公共函数访问。

##### 封装一个私有变量另一个例子：用JavaScript创建一个计数器：

```
'use strict';
function create_counter(initial) {
    var x = initial || 0;
    return {
        inc: function () {
            x += 1;
            return x;
        }
    }
}
复制代码
```

运行结果：

```
var c1 = create_counter();
c1.inc(); // 1
c1.inc(); // 2
c1.inc(); // 3

var c2 = create_counter(10);
c2.inc(); // 11
c2.inc(); // 12
c2.inc(); // 13
复制代码
```

在返回的对象中，实现了一个闭包，该闭包携带了局部变量x，并且，从外部代码根本无法访问到变量x。换句话说， **闭包就是携带状态的函数，并且它的状态可以完全对外隐藏起来。**

**4. 闭包应用场景之为节点循环绑定click事件**

```
function count() {
    var arr = [];
    for (var i=1; i<=3; i++) {
        arr.push(function () {
            return i * i;
        });
    }
    return arr;
}

var results = count();
var f1 = results[0];
var f2 = results[1];
var f3 = results[2];
```

在上面的例子中，每次循环，都创建了一个新的函数，然后，把创建的3个函数都添加到一个Array中返回了。

你可能认为调用f1()，f2()和f3()结果应该是 `1 4 9`， 但实际结果是：

```
f1(); // 16
f2(); // 16
f3(); // 16
复制代码
```

全部都是16！原因就在于返回的函数引用了变量i，但它并非立刻执行。等到3个函数都返回时，它们所引用的变量i已经变成了4，因此最终结果为16。

**返回闭包时牢记的一点就是：返回函数不要引用任何循环变量，或者后续会发生变化的变量**



### 展鸿科技

1.js怎么实现继承

2.super是什么

3.vue的生命周期

4.h5显示页面api
- 当用户最小化窗口或切换到另一个选项卡时，API会发送visibilitychange事件，让监听者知道页面状态已更改。例如，如果您的网络应用正在播放视频，则可以在用户将标签放入背景时暂停视频，并在用户返回标签时恢复播放。 用户不会在视频中丢失位置，视频的音轨不会干扰新前景选项卡中的音频，并且用户在此期间不会错过任何视频。
- 使用情景
  - 网站有图片轮播效果，只有在用户观看轮播的时候，才会自动展示下一张幻灯片。
  - 显示信息仪表盘的应用程序不希望在页面不可见时轮询服务器进行更新。
  - 页面想要检测是否正在渲染，以便可以准确的计算网页浏览量
  - 当设备进入待机模式时，网站想要关闭设备声音（用户按下电源键关闭屏幕）
- Document.hidden 只读 如果页面处于被认为是对用户隐藏状态时返回true，否则返回false。

5.html5和html4及html的区别

6.说说你知道的es6

7.html5为什么不用doctype
- <!DOCTYPE>只是一个说明，用来告诉浏览器当前的html页面是用什么版本的html写的。 html4.01的<!DOCTYPE>引用了DTD（document type define），因为html4.01是基于SGML的，而它引用的DTD指明了html的规则，从而浏览器能正确的渲染页面。而html5不是基于SGML所以不需要引用DTD. SGML，即一般标准标记语言，是一个用于定义文档标记语言标准的集合。

8.get和post的区别

9.call，apply和bind

10.如何学习前端

11.html5表单元素信息
- datalist 
  - datalist 元素规定输入域的选项列表。

      Webpage: <input type="url" list="url_list" name="link" />
      <datalist id="url_list">
      <option label="W3School" value="http://www.W3School.com.cn" />
      <option label="Google" value="http://www.google.com" />
      <option label="Microsoft" value="http://www.microsoft.com" />
      </datalist>
- keygen
  - 提供一种验证用户的可靠方法。
  - keygen 元素是密钥对生成器（key-pair generator）。当提交表单时，会生成两个键，一个是私钥，一个公钥。
  - 私钥（private key）存储于客户端，公钥（public key）则被发送到服务器。公钥可用于之后验证用户的客户端证书（client certificate）。
- output
  - 用于不同类型的输出，比如计算或脚本输出：

12.localStorage和sessionStorage，cookie之间的区别

13.vue什么情况下会发生内存泄露
  - v-if指令产生的内存泄露

  常见的内存泄露场景
  
    （1）监听在window/body等事件没有解绑

    （2）绑在EventBus的事件没有解绑

    （3）Vuex的$store watch了之后没有unwatch

    （4）模块形成的闭包内部变量使用完后没有置成null

    （5）使用第三方库创建，没有调用正确的销毁函数

14.深拷贝和浅拷贝

### 象限科技公司
自我介绍，面试官介绍公司情况。