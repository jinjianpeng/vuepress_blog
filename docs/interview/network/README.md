# 计算机网络
## tcp的三次握手和四次挥手
### 三次握手
![three_hander](/vuepress_blog/img/three_hander.jpg)
**为什么TCP客户端最后还要发送一次确认呢？**
一句话，主要防止已经失效的连接请求报文突然又传送到了服务器，从而产生错误。
如果使用的是两次握手建立连接，假设有这样一种场景，客户端发送了第一个请求连接并且没有丢失，只是因为在网络结点中滞留的时间太长了，由于TCP的客户端迟迟没有收到确认报文，以为服务器没有收到，此时重新向服务器发送这条报文，此后客户端和服务器经过两次握手完成连接，传输数据，然后关闭连接。此时此前滞留的那一次请求连接，网络通畅了到达了服务器，这个报文本该是失效的，但是，两次握手的机制将会让客户端和服务器再次建立连接，这将导致不必要的错误和资源的浪费。
如果采用的是三次握手，就算是那一次失效的报文传送过来了，服务端接受到了那条失效报文并且回复了确认报文，但是客户端不会再次发出确认。由于服务器收不到确认，就知道客户端并没有请求连接。
### 四次挥手
![four_swing_hander](/vuepress_blog/img/four_swing_hander.jpg)
1. 客户端进程发出连接释放报文，并且停止发送数据。释放数据报文首部，FIN=1，其序列号为seq=u（等于前面已经传送过来的数据的最后一个字节的序号加1），此时，客户端进入FIN-WAIT-1（终止等待1）状态。 TCP规定，FIN报文段即使不携带数据，也要消耗一个序号。
2. 服务器收到连接释放报文，发出确认报文，ACK=1，ack=u+1，并且带上自己的序列号seq=v，此时，服务端就进入了CLOSE-WAIT（关闭等待）状态。TCP服务器通知高层的应用进程，客户端向服务器的方向就释放了，这时候处于半关闭状态，即客户端已经没有数据要发送了，但是服务器若发送数据，客户端依然要接受。这个状态还要持续一段时间，也就是整个CLOSE-WAIT状态持续的时间。
3. 客户端收到服务器的确认请求后，此时，客户端就进入FIN-WAIT-2（终止等待2）状态，等待服务器发送连接释放报文（在这之前还需要接受服务器发送的最后的数据）。
4. 服务器将最后的数据发送完毕后，就向客户端发送连接释放报文，FIN=1，ack=u+1，由于在半关闭状态，服务器很可能又发送了一些数据，假定此时的序列号为seq=w，此时，服务器就进入了LAST-ACK（最后确认）状态，等待客户端的确认。
5. 客户端收到服务器的连接释放报文后，必须发出确认，ACK=1，ack=w+1，而自己的序列号是seq=u+1，此时，客户端就进入了TIME-WAIT（时间等待）状态。注意此时TCP连接还没有释放，必须经过2∗ *∗MSL（最长报文段寿命）的时间后，当客户端撤销相应的TCB后，才进入CLOSED状态。
6. 服务器只要收到了客户端发出的确认，立即进入CLOSED状态。同样，撤销TCB后，就结束了这次的TCP连接。可以看到，服务器结束TCP连接的时间要比客户端早一些。
> 为什么客户端最后还要等待2MSL？

**MSL（Maximum Segment Lifetime），TCP允许不同的实现可以设置不同的MSL值。**
  - 第一，保证客户端发送的最后一个ACK报文能够到达服务器，因为这个ACK报文可能丢失，站在服务器的角度看来，我已经发送了FIN+ACK报文请求断开了，客户端还没有给我回应，应该是我发送的请求断开报文它没有收到，于是服务器又会重新发送一次，而客户端就能在这个2MSL时间段内收到这个重传的报文，接着给出回应报文，并且会重启2MSL计时器。
  - 第二，防止类似与“三次握手”中提到了的“已经失效的连接请求报文段”出现在本连接中。客户端发送完最后一个确认报文后，在这个2MSL时间中，就可以使本连接持续的时间内所产生的所有报文段都从网络中消失。这样新的连接中不会出现旧连接的请求报文。

**为什么建立连接是三次握手，关闭连接确是四次挥手呢？**
 - 建立连接的时候， 服务器在LISTEN状态下，收到建立连接请求的SYN报文后，把ACK和SYN放在一个报文里发送给客户端。
  - 而关闭连接时，服务器收到对方的FIN报文时，仅仅表示对方不再发送数据了但是还能接收数据，而自己也未必全部数据都发送给对方了，所以己方可以立即关闭，也可以发送一些数据给对方后，再发送FIN报文给对方来表示同意现在关闭连接，因此，己方ACK和FIN一般都会分开发送，从而导致多了一次。

**如果已经建立了连接，但是客户端突然出现故障了怎么办？**
  - TCP还设有一个保活计时器，显然，客户端如果出现故障，服务器不能一直等下去，白白浪费资源。服务器每收到一次客户端的请求后都会重新复位这个计时器，时间通常是设置为2小时，若两小时还没有收到客户端的任何数据，服务器就会发送一个探测报文段，以后每隔75秒发送一次。若一连发送10个探测报文仍然没反应，服务器就认为客户端出了故障，接着就关闭连接。


### 跨域

同源策略（ajax/fetch）：部署到web服务器上

跨域：协议号、主机号、端口号至少有一项不同
- 服务器拆分(现在部署的时候也要拆分服务器，这样整个项目就又处于跨域传输)
   - web服务器：静态资源 kbs.sports.com
   - data服务器：业务逻辑和数据分析 api.sports.com
   - 图片服务器 

常用的跨域请求解决方法
1.JSONP
- script
- img
- link
- iframe
![jsonp](/vuepress_blog/img/jsonp.png)

**代码实现**

*html*
```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="./jsonp.js"></script>
</body>
</html>
```
*ajax*
```
$.ajax({
    url:'http://127.0.0.1:10000/list',
    dataType:"jsonp",
    method:'get',
    success:function(data){
        console.log(data);
    },
    fail:function(err){
        console.log(err);
    }
})
```
*express*
```
const express = require('express');
let app =  express();
app.listen(10000,_=>{
    console.log('OK!');
})
app.get("/list",(req,res)=>{
    let {callback} = req.query;
    let data={
        id:0,
        msg:'hello jsonp'
    };
    res.send(`${callback}(${JSON.stringify(data)})`);

})
```
jsonp的问题：
 - 只能处理GET请求

 2. CORS跨域资源共享
 浏览器将CORS请求分成两类：简单请求（simple request）和非简单请求（not-so-simple request）。
 只要同时满足以下两大条件，就属于简单请求。
 ```
（1) 请求方法是以下三种方法之一：

HEAD
GET
POST
（2）HTTP的头信息不超出以下几种字段：

Accept
Accept-Language
Content-Language
Last-Event-ID
Content-Type：只限于三个值application/x-www-form-urlencoded、multipart/form-data、text/plain
 ```

 这是为了兼容表单（form），因为历史上表单一直可以发出跨域请求。AJAX 的跨域设计就是，只要表单可以发，AJAX 就可以直接发。

凡是不同时满足上面两个条件，就属于非简单请求。

浏览器对这两种请求的处理，是不一样的。

实现过程
 - 客户端：发送ajax/fetch请求
 - 服务器：设置相关的头信息

 ### 3.http proxy =>webpack webpack-dev-server

 ### 4. nginx 反向代理

 ### 5.postMessage

 代码实现
 - 开启服务器127.0.0.1：2001
 - 开启服务器127.0.0.1：2002
 ```
const express = require('express');
let app =  express();
app.listen(2001,_=>{
    console.log('OK!');
});
app.use(express.static('./'));
 ```
 - 客户端：设置iframe框架，并向iframe传递数据 A页面向B页面传递数据

 **A.html**
 ```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <iframe src="http://127.0.0.1:2002/B.html" id="iframe" frameborder="0" ></iframe>
    <script>
        iframe.onload=function(){
            // console.log(iframe.contentWindow);
            iframe.contentWindow.postMessage("hello postmessage",'http://127.0.0.1:2002/');
        }
        window.onmessage=function(event){
            console.log(event.data);
        }
    </script>
</body>
</html>
 ```
 **B.html**
 ```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <script>
        window.onmessage=function(event){
            let data = event.data;
            console.log(data);
            event.source.postMessage(data+'333',event.origin);
        }
    </script>
</body>
</html>
 ```
 ### 6.websocket协议跨域

 ### 7.document.domain + iframe
 只能实现：同一个主域，不同子域之间的操作

 ### 8.window.name + iframe
 

 ## http中put和post的区别

  - 有的观点认为，应该用POST来创建一个资源，用PUT来更新一个资源；有的观点认为，应该用PUT来创建一个资源，用POST来更新一个资源；还有的观点认为可以用PUT和POST中任何一个来做创建或者更新一个资源。这些观点都只看到了风格，争论起来也只是争论哪种风格更好，其实，用PUT还是POST，不是看这是创建还是更新资源的动作，这不是风格的问题，而是语义的问题。

在HTTP中，PUT被定义为idempotent的方法，POST则不是，这是一个很重要的区别。

 > “Methods can also have the property of "idempotence" in that (aside from error or expiration issues) the side-effects of N > 0 identical requests is the same as for a single request.”

上面的话就是说，如果一个方法重复执行多次，产生的效果是一样的，那就是idempotent的。

<font color="red">举一个简单的例子，假如有一个博客系统提供一个Web API，模式是这样http://superblogging/blogs/post/{blog-name}，很简单，将{blog-name}替换为我们的blog名字，往这个URI发送一个HTTP PUT或者POST请求，HTTP的body部分就是博文，这是一个很简单的REST API例子。我们应该用PUT方法还是POST方法？取决于这个REST服务的行为是否是idempotent的，假如我们发送两个http://superblogging/blogs/post/Sample请求，服务器端是什么样的行为？如果产生了两个博客帖子，那就说明这个服务不是idempotent的，因为多次使用产生了副作用了嘛；如果后一个请求把第一个请求覆盖掉了，那这个服务就是idempotent的。前一种情况，应该使用POST方法，后一种情况，应该使用PUT方法。</font>

也许你会觉得这个两个方法的差别没什么大不了的，用错了也不会有什么问题，但是你的服务一放到internet上，如果不遵从HTTP协议的规范，就可能给自己带来麻烦。比如，没准Google Crawler也会访问你的服务，如果让一个不是indempotent的服务可以用indempotent的方法访问，那么你服务器的状态可能就会被Crawler修改，这是不应该发生的。