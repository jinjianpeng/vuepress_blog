# 鼠标事件

## 鼠标事件的种类

- click：按下鼠标（通常是按下主按钮）时触发。
- dblclick：在同一个元素上双击鼠标时触发。
- mousedown：按下鼠标键时触发。
- mouseup：释放按下的鼠标键时触发。
- mousemove：当鼠标在一个节点内部移动时触发。当鼠标持续移动时，该事件会连续触发。为了避免性能问题，建议对该事件- 的监听函数做一些限定，比如限定一段时间内只能运行一次。
- mouseenter：鼠标进入一个节点时触发，进入子节点不会触发这个事件（详见后文）。
- **mouseover：鼠标进入一个节点时触发，进入子节点会再一次触发这个事件（详见后文）**。
- mouseout：鼠标离开一个节点时触发，离开父节点也会触发这个事件（详见后文）。
- mouseleave：鼠标离开一个节点时触发，离开父节点不会触发这个事件（详见后文）。
- contextmenu：按下鼠标右键时（上下文菜单出现前）触发，或者按下“上下文菜单键”时触发。
- wheel：滚动鼠标的滚轮时触发，该事件继承的是WheelEvent接口。
</hr></br>

`click事件`指的是，用户在同一个位置先完成mousedown动作，再完成mouseup动作。因此，触发顺序是，mousedown首先触发，mouseup接着触发，click最后触发。

`dblclick事件`则会在mousedown、mouseup、click之后触发。

**`mouseover事件`和`mouseenter事件`**，都是鼠标进入一个节点时触发。两者的区别是，mouseenter事件只触发一次，而只要鼠标在节点内部移动，mouseover事件会在子节点上触发多次。

**`mouseout事件`和`mouseleave事件`**，都是鼠标离开一个节点时触发。两者的区别是，在父元素内部离开一个子元素时，mouseleave事件不会触发，而mouseout事件会触发。

> 图解

![mouseenter](https://developer.mozilla.org/@api/deki/files/5908/=mouseenter.png)

(mouseenter事件)触发时，会向层次结构的每个元素发送一个mouseenter事件。当指针到达文本时，此处将4个事件发送到层次结构的四个元素。

![mouseover](https://developer.mozilla.org/@api/deki/files/5909/=mouseover.png)

一个单独的mouseover事件被发送到DOM树的最深层元素，然后它将层次结构向上冒泡，直到它被处理程序取消或到达根目录。

<pre style="color:white;">
对于深层次结构，发送的mouseenter事件数量可能非常大并且会导致严重的性能问题。在这种
情况下，最好是监听鼠标悬停事件。
（可使用chrome开发者工具选项卡中的Performance进行性能测试）
结合其对称事件, mouseleave, mouseenter DOM事件的行为方式与CSS  :hover 伪类非常相似。.
</pre>

<b style="font-size:'25px';border-bottom:2px solid #ccc;font-weight:700;color:red;padding:5px 0;">注意：</b>
<div  style="border:3px solid #ccc;margin-top:5px;padding:25px;font-size:16px">
深入研究鼠标在元素之间移动时发生的事件。
- 事件 mouseover/mouseout，relatedTarget
- 跳过元素
- 当移动到一个子元素时 mouseout
- 事件 mouseenter 和 mouseleave
- 事件委托<a style="display:block;" target="_blank" href="https://zh.javascript.info/mousemove-mouseover-mouseout-mouseenter-mouseleave">详细请看博客</a>

</div>

# 定位z-index比较

## 在CSS2.1的年代，在CSS3还没有出现的时候（注意这里的前提），层叠顺序

![层叠顺序](https://image.zhangxinxu.com/image/blog/201601/2016-01-07_223349.png)

![解释层叠关系](https://image.zhangxinxu.com/image/blog/201601/2016-01-07_235108.png)

**诸如border/background一般为装饰属性，而浮动和块状元素一般用作布局，而内联元素都是内容。网页中最重要的是什么？当然是内容了哈，对不对！**

因此，一定要让内容的层叠顺序相当高，当发生层叠是很好，重要的文字啊图片内容可以优先暴露在屏幕上。

## CSS3年代
> 层叠领域的黄金准则

1. **谁大谁上**：当具有明显的层叠水平标示的时候，如识别的z-indx值，在同一个层叠上下文领域，层叠水平值大的那一个覆盖小的那一个。通俗讲就是官大的压死官小的。
2. **后来居上**：当元素的层叠水平一致、层叠顺序相同的时候，在DOM流中处于后面的元素会覆盖前面的元素。

形象解释：拼爹
注意： z-index 进行定位元素(position:absolute, position:relative, or position:fixed)。

详细请看<a target="_blank" href="https://www.zhangxinxu.com/wordpress/2016/01/understand-css-stacking-context-order-z-index/">张鑫旭</a>

# position定位
## 认知错误1

> position的定位依据是根据**最近**上级**非标准流**元素来偏移位置

- 如果最近上级元素是绝对定位，则自己按照绝对定位作为标准设置偏移， <span style="color:red">而不是必须以相对定位为标准</span>
 

# IndexedDB

## 兼容性

> window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;

## API

<APIList title="IndexedDB" apiul='[{"key":"open","value":"open()函数第一个参数是数据库名称，第二参数是版本号"}]'/>


# localStroge

# fileReader


# setAttribute 和 style设置css属性区别

- setAttribute 适用于HTML属性，也适用于自定义属性。setAttribute(property,value),参数都是字符串。
- style 设置element内联属性中style属性值。使用驼峰法设置样式。

常见HTML属性
- class	规定元素的类名（classname）
- data-*	用于存储页面的自定义数据
- hidden	hidden 属性规定对元素进行隐藏。
- id	规定元素的唯一 id
- style	规定元素的行内样式（inline style）
- title	规定元素的额外信息（可在工具提示中显示）
因此，setAttribute比较适合设置自定义属性。style设置元素自带属性。

# DOM2 Style规范
- cssText 包含style属性中的css代码，可以存取样式的css代码。读模式下，cssText返回style属性CSS代码载浏览器内部的表示。在写模式下，给cssText赋值会重写整个style属性的值，意味着之前通过style属性设置的属性都会消失。


# 获取元素CSS值之getComputedStyle方法
> getComputedStyle是一个可以获取当前元素所有最终使用的CSS属性值。返回的是一个CSS样式声明对象([object CSSStyleDeclaration])，只读。

语法如下：
`var style = window.getComputedStyle("元素", "伪类");`

例如：

```js
var dom = document.getElementById("test"),
    style = window.getComputedStyle(dom , ":after");
```

## getComputedStyle与style的区别

我们使用element.style也可以获取元素的CSS样式声明对象，但是其与getComputedStyle方法还有有一些差异的。

1. **只读与可写**
  
   正如上面提到的getComputedStyle方法是只读的，只能获取样式，不能设置；而element.style能读能写，能屈能伸。

2. **获取的对象范围**

   getComputedStyle方法获取的是最终应用在元素上的所有CSS属性对象（即使没有CSS代码，也会把默认的祖宗八代都显示出来）；而element.style只能获取元素style属性中的CSS样式。因此对于一个光秃秃的元素.p，getComputedStyle方法返回对象中length属性值（如果有）就是190+(据我测试FF:192, IE9:195, Chrome:253, 不同环境结果可能有差异), 而element.style就是0。style只能获取内敛属性。

## getComputedStyle与defaultView

  jQuery源代码，会发现，其css()方法实现不是使用的window.getComputedStyle而是document.defaultView.getComputedStyle

## getComputedStyle与currentStyle

   currentStyle是IE浏览器自娱自乐的一个属性，其与element.style可以说是近亲，至少在使用形式上类似，element.currentStyle，差别在于element.currentStyle返回的是元素当前应用的最终CSS属性值（包括外链CSS文件，页面中嵌入的`<style>`属性等）。

<Notice text="不过，currentStyle属性貌似不支持伪类样式获取，这是与getComputedStyle方法的差异，也是jQuery css()方法无法体现的一点。"/>

## getPropertyValue方法

`window.getComputedStyle(element, null).getPropertyValue("float");`

如果我们不使用getPropertyValue方法，直接使用键值访问，其实也是可以的。但是，比如这里的的float，如果使用键值访问，则不能直接使用getComputedStyle(element, null).float，而应该是cssFloat与styleFloat，自然需要浏览器判断了，比较折腾！

## getPropertyValue和getAttribute
在老的IE浏览器（包括最新的），getAttribute方法提供了与getPropertyValue方法类似的功能，可以访问CSS样式对象的属性。用法与getPropertyValue类似：

<Notice  text="注意到没，使用getAttribute方法也不需要cssFloat与styleFloat的怪异写法与兼容性处理。不过，还是有一点差异的，就是属性名需要驼峰写法，如下："/>

`style.getAttribute("backgroundColor");`

# js 获取屏幕、浏览器、页面的高度宽度

## 展示图
![图示](https://images2015.cnblogs.com/blog/153475/201512/153475-20151222173139109-87271821.png)

<Notice  text="图中的body根据HTML的文档渲染模式不同指定的body也不同。"/>

```js
// 标准模式时(document.compatMode == 'CSS1Compat')，body = document.documentElement
var body = (document.compatMode && document.compatMode == 'CSS1Compat') ? document.documentElement : document.body;
```

## 屏幕信息
![图示](https://images2015.cnblogs.com/blog/153475/201512/153475-20151231161329104-781827200.png)


<APIList title="screen" apiul='[{"key":"screen.height","value":"屏幕高度。"},{"key":"screen.width","value":"屏幕宽度。"},{"key":"screen.availHeight","value":"屏幕可用高度。即屏幕高度减去上下任务栏后的高度，可表示为软件最大化时的高度。"},{"key":"screen.availWidth","value":"屏幕可用宽度。即屏幕宽度减去左右任务栏后的宽度，可表示为软件最大化时的宽度。"},{"key":"任务栏高/宽度","value":"可通过屏幕高/宽度 减去 屏幕可用高/宽度得出。如：任务栏高度 = screen.height - screen.availHeight 。"}]'/>

## 浏览器信息
![图示](https://images2015.cnblogs.com/blog/153475/201512/153475-20151231163332870-1588713430.png)

- window.outerHeight ：浏览器高度。
- window.outerWidth ：浏览器宽度。
- window.innerHeight ：浏览器内页面可用高度；此高度包含了水平滚动条的高度(若存在)。可表示为浏览器当前高度去除浏览器边框、工具条后的高度。
- window.innerWidth ：浏览器内页面可用宽度；此宽度包含了垂直滚动条的宽度(若存在)。可表示为浏览器当前宽度去除浏览器边框后的宽度。
- 工具栏高/宽度 ：包含了地址栏、书签栏、浏览器边框等范围。如：高度，可通过浏览器高度 - 页面可用高度得出，即：window.outerHeight - - window.innerHeight。

## 页面信息
![图示](https://images2015.cnblogs.com/blog/153475/201512/153475-20151231171044510-8761357.png)

- body.offsetHeight ：body总高度。
- body.offsetWidth ：body总宽度。
- body.clientHeight ：body展示的高度；表示body在浏览器内显示的区域高度。
- body.clientWidth ：body展示的宽度；表示body在浏览器内显示的区域宽度。
- 滚动条高度/宽度 ：如高度，可通过浏览器内页面可用高度 - body展示高度得出，即window.innerHeight - body.clientHeight。



# 只有函数对象有 prototype 属性（一般对象自己加的不算）

# Vue中父子组件挂载顺序

![挂载顺序](https://segmentfault.com/img/bVbePUv?w=302&h=298)

**父组件先创建，然后子组件创建；子组件先挂载，然后父组件挂载。**
**兄弟组件创建和挂载顺序按照父组件引入顺序为准。**

# select dom获取value --- 通过selectDom.value


# javascript sort 两个数组依次其中一个排序

> 例子
   itemsArray = [ 
    ['Anne', 'a'],
    ['Bob', 'b'],
    ['Henry', 'b'],
    ['Andrew', 'd'],
    ['Jason', 'c'],
    ['Thomas', 'b']
]
// 匹配数组
sortingArr = [ 'b', 'c', 'b', 'b', 'a', 'd' ]
//要求排序后
itemsArray = [    
    ['Bob', 'b'],
    ['Jason', 'c'],
    ['Henry', 'b'],
    ['Thomas', 'b']
    ['Anne', 'a'],
    ['Andrew', 'd'],
]

## 方法一

```js
itemsArray.sort(function(a, b){  
  return sortingArr.indexOf(a) - sortingArr.indexOf(b);
});

```

## 方法二

```js
result = []

sorting.forEach(function(key) {
    var found = false;
    items = items.filter(function(item) {
        if(!found && item[1] == key) {
            result.push(item);
            found = true;
            return false;
        } else 
            return true;
    })
})

result.forEach(function(item) {
    document.writeln(item[0]) /// Bob Jason Henry Thomas Andrew
})

```

# 数据类型转换
## 其他类型与Boolean类型转换

<pre style="color:white;">
其他类型值	转换成的布尔值
undefined	false
null	        false
布尔值	        不用转换
数字	        0，NaN转化成false，其他数字类型转换成true
字符串	        只有空字符串''转换成false，其他都转换成true
对象	        全部转换为true,包括[]
</pre>

## 隐式类型转换

> 重点说明： 在null、undefined进行"=="比较时不进行类型转换（但是ECMA规范中定义null==undefined // true ），其他运算操作时会转换。

> ==（抽象比较运算符）

**抽象比较运算符在比较它们之前在类型之间进行自动转换**

> \+ (加法运算符)


## 引用类型转成原始类型分析

在JS内部定义了4个抽象操作(只能供JS内部使用，我们用户无法操作)
- ToPrimitive(obj, [PreferredType])
    obj:待转换的对象
    PreferredType：待转成的目标类型(只接受Number、String两种类型，默认是空)，如果obj是Date或symbol,则PreferredType=String,其余的情况下PreferredType=Number

    **如果PreferredType=Number,引用类型转化为数字**
    
    1. 调用valueOf(),如果返回原始值，则结束，如果返回非原始值，则执行第2步
    2. 调用toString(),如果返回原始值，则结束，如果返回非原始值，则报错

    **如果PreferredType=String,引用类型转换为字符串**

    1. 调用toString(),如果返回原始值，则结束，如果返回非原始值，则执行第2步
    2. 调用valueOf(),如果返回原始值，则结束，如果返回非原始值，则报错

    引用类型转换为布尔类型，始终返回true

- ToNumber(arguments)
- ToString(arguments)
- ToBoolean(arguments)

在执行隐式类型转换时，会自动调用这四个内部方法
<p style="color:red;font-weight:700;">原始值之间相互转换：ToNumber()、ToString()、ToBoolean()<p>
<p style="color:red;font-weight:700;">引用值到原始值转换: ToPrimitive()<p>

在执行显示类型转换时，如Number()、String()、Boolean()时，内部还是调用ToNumber()、ToString()、ToBoolean()，如果待转换的是引用类型，则先执行ToPrimitive()，得到原始值，再执行ToNumber()、ToString()、ToBoolean()

> 这里特别说明：把引用值转原始值的过程中JS内部在调用ToPrimitive()方法时，分为两种情况
1、明确指定目标转换类型：按照指定类型去调用toString()/valueOf()方法
2、没有指定目标转换类型：按照obj是否是Date类型，去调用toString()/valueOf()方法


![类型转换](https://segmentfault.com/img/bV38Bp?w=538&h=545)
