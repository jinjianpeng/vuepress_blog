# elementUI
## bug
对于Vue实例中的对象，要初始化。比如
```html
 <el-form :inline="true" :model="addstudent" class="demo-form-inline">
   <el-divider content-position="left">基本信息</el-divider>
   <el-form-item label="学号：">
      <el-input v-model="addstudent.snumber" placeholder="亲输入学号"></el-input>
   </el-form-item>
   <el-form-item label="姓名：">
      <el-input v-model="addstudent.name" placeholder="亲输入姓名"></el-input>
   </el-form-item>
   <el-form-item label="班级：">
      <el-input v-model="addstudent.class" placeholder="请输入班级"></el-input>
   </el-form-item>
   <el-form-item label="性别：">
      <el-radio v-model="addstudent.sex" label="男">男</el-radio>
      <el-radio v-model="addstudent.sex" label="女">女</el-radio>
   </el-form-item>
   <el-divider content-position="left">成绩信息</el-divider>

   <el-form-item label="c语言：">
      <el-input v-model="addstudent.clan"></el-input>
   </el-form-item>
   <el-form-item label="java语言：">
      <el-input v-model="addstudent.javalan"></el-input>
   </el-form-item>
   <el-form-item label="数据库：">
      <el-input v-model="addstudent.dblan"></el-input>
   </el-form-item>
</el-form>
```
```js
addstudent: {
      snumber: "",
      name: '',
      class: '',
      clan: '',
      javalan: '',
      dblan: ''
},
```
而不能把addstudent:{}当作初始化。如果你怎么做了，你在表单输入内容无法得到双向数据更新，因为不是触发事件change。
