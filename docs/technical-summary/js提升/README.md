## 关于this
- 1.this最终指向的是调用它的对象(这个函数中包含多个对象，尽管这个函数是被最外层的对象所调用，this指向的也只是它上一级的对象)
```js
function fn()  
{  
    this.user = '追梦子';  
    return undefined;
}
var a = new fn;  
console.log(a.user); //追梦子
```
```js
function fn()  
{  
    this.user = '追梦子';  
    return {};  
}
var a = new fn;  
console.log(a.user); //undefined
```
- 2.如果返回值是一个对象，那么this指向的就是那个返回的对象，如果返回值不是一个对象那么this还是指向函数的实例。
还有一点就是虽然null也是对象，但是在这里this还是指向那个函数的实例，因为null比较特殊。
```js
function fn()  
{  
    this.user = '追梦子';  
    return null;
}
var a = new fn;  
console.log(a.user); //追梦子
```
- 3.new操作符会改变函数this的指向问题
```js
function fn(){
    this.num = 1;
}
var a = new fn();
console.log(a.num); //1
```
为什么this会指向a？首先new关键字会创建一个空的对象，然后会自动调用一个函数，将this指向这个空对象，这样的话函数内部的this就会被这个空的对象替代。
> 注意
```js
var People = function(){
var name = "张三"; //局部变量
var age = 24;

this.getname =function(){ //局部方法
return this.name;
}
}
var p1 = new People()
alert(p1.getname()) //undefined？？
```
new的时候，p1实例只克隆this属性和方法，不会克隆局部变量

## call,apply,bind
首先先看一个例子：
```js
var a={
  user:'zhangsan',
  fn:function(){
    console.log(this.user);//zhangsan
  }
}
var b = a.fn;
b.call(a);//调用 b在a的环境下调用
b.apply(a);//应用 b在a的环境下应用
```
**在只传一个参数的情况下，call和apply都是改变this的指向，并调用b。第一个参数表示调用方（指函数this指向），就是说调用方调用那个被调用的函数。**
```js
var a = {
    user:"zhangsan",
    fn:function(e,ee){
        console.log(this.user); //zhangsan
        console.log(e+ee); //3
    }
}
var b = a.fn;
b.call(a,1,2);
```
```js
var a = {
    user:"zhangsan",
    fn:function(e,ee){
        console.log(this.user); //zhangsan
        console.log(e+ee); //11
    }
}
var b = a.fn;
b.apply(a,[10,1]);
```
**call的还可以添加多个值参数都传入函数中，而apply只有第二参数传递一个数组。**
> 注意如果call和apply的第一个参数写的是null，那么this指向的是window对象

**bind()**
```js
var a = {
    user:"zhangsan",
    fn:function(){
        console.log(this.user);
    }
}
var b = a.fn;
b.bind(a);

```
我们发现代码没有被打印，对，这就是bind和call、apply方法的不同，<font color="red">实际上bind方法返回的是一个修改过后的函数。</font>
```js
var a = {
    user:"zhangsan",
    fn:function(e,d,f){
        console.log(this.user); //zhangsan
        console.log(e,d,f); //10 1 2
    }
}
var b = a.fn;
var c = b.bind(a,10);
c(1,2);
```
同样bind也可以有多个参数，并且参数可以执行的时候再次添加，但是要注意的是，<font color="red">参数是按照形参的顺序进行的。传参方式和call相似。</font>

## js继承
```js
//js继承的几种方式
// 1.原型链继承
function Person(){
    this.hobby= [];
}
Person.prototype.hello = function(){
    console.log("hello "+this.name);
}
function Student(name){
    this.name = name;
}
Student.prototype = new Person(); //改变原型的指向
const stu1 = new Student("张三");
const stu2 = new Student("李四");
stu1.hobby.push('乒乓');
console.log(stu1.hobby); //[ '乒乓' ] **********************
console.log(stu2.hobby); //[ '乒乓' ]
//缺点 1.引用缺陷， 父类构造方法的引用属性对每个student实例都共享 2.Student的原型的constructor指向父类的构造方法。

// 2. 借用继承
function Person(name){
    this.hobby= [];
    this.name = name;
}
Person.prototype.hello = function(){
    console.log("hello "+this.name);
}
function Student(name,age){
    //1.new的时候，创建一个空对象给实例，并把this指向给实例。
    //2.所以这里this就是那个实例对象。
    //3.它调用调用父类的构造函数。
    Person.call(this,name); 
    this.age = age;
}
const stu1 = new Student("张三",15);
const stu2 = new Student("李四",25);
stu1.hobby.push("篮球");
console.log(stu1.name,stu1.age,stu1.hobby);//张三 15 [ '篮球' ]
console.log(stu2.name,stu2.age,stu2.hobby);//李四 25 []

stu1.hello(); //TypeError: stu1.hello is not a function ******************
// 缺点：无法继承原型链的属性和方法。也就是只能把函数和属性都写入构造函数里，不利于函数复用。
//3.组合继承(原型链+借用)
function Person(name){
    this.hobby= [];
    this.name = name;
}
Person.prototype.hello = function(){
    console.log("hello "+this.name);
}
function Student(name,age){
    Person.call(this,name); //第二次调用构造函数
    this.age = age;
}
// 读取对象的属性值，先从实例属性查找，没有找到在从原型链找。
Student.prototype = new Person("被覆盖"); //第一次调用父类构造函数
const stu1 = new Student("张三",19);
const stu2 = new Student("李四",24);
stu1.hobby.push("篮球");
stu1.hello(); //hello 张三
console.log(stu1.name,stu1.age,stu1.hobby);//张三 19 [ '篮球' ]
console.log(stu2.name,stu2.age,stu2.hobby);//李四 24 []
// 结合两种方法的优点，解决了缺点，实现了继承，但是它一共调用了2次的父类的构造函数。

/*
  对象字面量的继承
*/

// 1. 原型继承
const person = {
    name:"王五",
    hobby:['羽毛球'],
    hello:function(){
        console.log("hello "+this.name);
    }
}
//定义一个浅克隆，克隆对象复制到原型上。
function clone(object){
    function f(){};
    f.prototype = object;
    return new f();
}
//var clo1 = clone(person);
//console.log(clo1.name,clo1.hobby); //王五 [ '羽毛球' ]
//clo1.hello(); //hello 王五
// 2.寄生继承
const person = {
    name:"王五",
    hobby:['羽毛球'],
    hello:function(){
        console.log("hello "+this.name);
    }
}
function createAnother(obj){
    var another = clone(obj);  //创建对象
    another.cry = function(){  //添加功能
        console.log('5555');
    }
    return another;
}
var another = createAnother(person);
console.log(another.name,another.hobby); //王五 [ '羽毛球' ]
another.cry(); //5555

// 4.寄生组合继承 (继承原型方式采用复制)
/*
 定义一个继承函数
  student: 子类构造函数
  person : 父类原型
 */
function inherit(student,person){
    var temp = Object.create(person); //克隆原型
    temp.constructor = student;       //增强模式
    student.prototype = temp;        // 原型赋值
}
function Person(name){
    this.hobby= [];
    this.name = name;
}
Person.prototype.hello = function(){
    console.log("hello "+this.name);
}
function Student(name,age){
    Person.call(this,name);
    this.age = age;
}
inherit(Student,Person.prototype);
const stu1 = new Student("张三",19);
const stu2 = new Student("李四",24);
stu1.hobby.push("篮球");
stu1.hello(); //hello 张三
console.log(stu1.name,stu1.age,stu1.hobby);//张三 19 [ '篮球' ]
console.log(stu2.name,stu2.age,stu2.hobby);//李四 24 []
// 最推荐的方式实现继承

//es6继承
class Person{
    constructor(name){
        this.name = name;
        this.hobby = [];
    }
    hello(){
        console.log("hello "+this.name);
    }
}
class Student extends Person{
    constructor(name,age){
        super(name);
        this.age = age;
    }
}
const stu1 = new Student("张三",18);
const stu2 = new Student("李四",24);
stu1.hobby.push("篮球");
stu1.hello(); //hello 张三
console.log(stu1.name,stu1.age,stu1.hobby);//张三 18 [ '篮球' ]
console.log(stu2.name,stu2.age,stu2.hobby);//李四 24 []


```

## class 的基本语法
### 1.简介
```js
class Point {
  // ...
}

typeof Point // "function"
Point === Point.prototype.constructor // true
```
上面代码表明，类的数据类型就是函数，类本身就指向构造函数。
- **事实上，类的所有方法都定义在类的prototype属性上面。**
```js

class Point {
  constructor() {
    // ...
  }

  toString() {
    // ...
  }

  toValue() {
    // ...
  }
}

// 等同于

Point.prototype = {
  constructor() {},
  toString() {},
  toValue() {},
};
```
在类的实例上面调用方法，其实就是调用原型上的方法。
另外，类的内部所有定义的方法，都是不可枚举的（non-enumerable）。
<hr>

#### constructor 方法 § 
constructor方法是类的默认方法，通过new命令生成对象实例时，自动调用该方法。一个类必须有constructor方法，如果没有显式定义，一个空的constructor方法会被默认添加。
constructor方法默认返回实例对象（即this），完全可以指定返回另外一个对象。
> 类必须使用new调用，否则会报错。这是它跟普通构造函数的一个主要区别，后者不用new也可以执行。

```js
class Foo {
  constructor() {
    return Object.create(null);
  }
}

Foo()
// TypeError: Class constructor Foo cannot be invoked without 'new'
```

实例，它们的原型都是Point.prototype，所以__proto__属性是相等的。

这也意味着，可以通过实例的__proto__属性为“类”添加方法。
```js
var p1 = new Point(2,3);
var p2 = new Point(3,2);

p1.__proto__.printName = function () { return 'Oops' };

p1.printName() // "Oops"
p2.printName() // "Oops"

var p3 = new Point(4,2);
p3.printName() // "Oops"
```
#### 取值函数（getter）和存值函数（setter） §
与 ES5 一样，在“类”的内部可以使用get和set关键字，对某个属性设置存值函数和取值函数，拦截该属性的存取行为。
```js
class MyClass {
  constructor() {
    // ...
  }
  get prop() {
    return 'getter';
  }
  set prop(value) {
    console.log('setter: '+value);
  }
}

let inst = new MyClass();

inst.prop = 123;
// setter: 123

inst.prop
// 'getter'
```
存值函数和取值函数是设置在属性的 Descriptor 对象上的。
```js
class CustomHTMLElement {
  constructor(element) {
    this.element = element;
  }

  get html() {
    return this.element.innerHTML;
  }

  set html(value) {
    this.element.innerHTML = value;
  }
}

var descriptor = Object.getOwnPropertyDescriptor(
  CustomHTMLElement.prototype, "html"
);

"get" in descriptor  // true
"set" in descriptor  // true
```
<hr>

#### Class 表达式  §
与函数一样，类也可以使用表达式的形式定义。
```js
const MyClass = class Me {
  getClassName() {
    return Me.name;
  }
};
```
上面代码使用表达式定义了一个类。需要注意的是，这个类的名字是Me，但是Me只在 Class 的内部可用，指代当前类。在 Class 外部，这个类只能用MyClass引用。
如果类的内部没用到的话，可以省略Me，也就是可以写成下面的形式
```js
const MyClass = class { /* ... */ };
```
<hr>

> 注意点 
**（1）严格模式**
类和模块的内部，默认就是严格模式，所以不需要使用use strict指定运行模式。只要你的代码写在类或模块之中，就只有严格模式可用。考虑到未来所有的代码，其实都是运行在模块之中，所以 ES6 实际上把整个语言升级到了严格模式。
**（2）不存在提升**
类不存在变量提升（hoist），这一点与 ES5 完全不同。
```js
new Foo(); // ReferenceError
class Foo {}
```
**（3）name 属性**
由于本质上，ES6 的类只是 ES5 的构造函数的一层包装，所以函数的许多特性都被Class继承，包括name属性。
```js
class Point {}
Point.name // "Point"
```
**(4)this 的指向**
类的方法内部如果含有this，它默认指向类的实例。但是，必须非常小心，一旦单独使用该方法，很可能报错。
```js
class Logger {
  printName(name = 'there') {
    this.print(`Hello ${name}`);
  }

  print(text) {
    console.log(text);
  }
}

const logger = new Logger();
const { printName } = logger;
printName(); // TypeError: Cannot read property 'print' of undefined
```
上面代码中，printName方法中的this，默认指向Logger类的实例。但是，如果将这个方法提取出来单独使用，this会指向该方法运行时所在的环境（由于 class 内部是严格模式，所以 this 实际指向的是undefined），从而导致找不到print方法而报错。

### 2. 静态方法
类相当于实例的原型，所有在类中定义的方法，都会被实例继承。如果在一个方法前，加上static关键字，就表示该方法不会被实例继承，而是直接通过类来调用，这就称为“静态方法”。
```js
class Foo {
  static classMethod() {
    return 'hello';
  }
}

Foo.classMethod() // 'hello'

var foo = new Foo();
foo.classMethod()
// TypeError: foo.classMethod is not a function
```
> 注意，如果静态方法包含this关键字，这个this指的是类，而不是实例。
```js
class Foo {
  static bar() {
    this.baz();
  }
  static baz() {
    console.log('hello');
  }
  baz() {
    console.log('world');
  }
}

Foo.bar() // hello
```
上面代码中，静态方法bar调用了this.baz，这里的this指的是Foo类，而不是Foo的实例，等同于调用Foo.baz。另外，从这个例子还可以看出，静态方法可以与非静态方法重名。

父类的静态方法，可以被子类继承。
```js
class Foo {
  static classMethod() {
    return 'hello';
  }
}

class Bar extends Foo {
}

Bar.classMethod() // 'hello'
```
### 3.实例属性的新写法
实例属性除了定义在constructor()方法里面的this上面，也可以定义在类的最顶层。
```js
class IncreasingCounter {
  constructor() {
    this._count = 0;
  }
  get value() {
    console.log('Getting the current value!');
    return this._count;
  }
  increment() {
    this._count++;
  }
}
```
上面代码中，实例属性this._count定义在constructor()方法里面。另一种写法是，这个属性也可以定义在类的最顶层，其他都不变。
```js
class IncreasingCounter {
  _count = 0;
  get value() {
    console.log('Getting the current value!');
    return this._count;
  }
  increment() {
    this._count++;
  }
}
```
上面代码中，实例属性_count与取值函数value()和increment()方法，处于同一个层级。这时，不需要在实例属性前面加上this。

这种新写法的好处是，所有实例对象自身的属性都定义在类的头部，看上去比较整齐，一眼就能看出这个类有哪些实例属性。
<hr>

### 4.静态属性
静态属性指的是 Class 本身的属性，即Class.propName，而不是定义在实例对象（this）上的属性。
```js
class Foo {
}

Foo.prop = 1;
Foo.prop // 1
```
上面的写法为Foo类定义了一个静态属性prop。

目前，只有这种写法可行，因为 ES6 明确规定，Class 内部只有静态方法，没有静态属性。现在有一个提案提供了类的静态属性，写法是在实例属性的前面，加上static关键字。

```js
// 老写法
class Foo {
  // ...
}
Foo.prop = 1;

// 新写法
class Foo {
  static prop = 1;
}
```
<hr>

### 5.私有方法和私有属性
#### 私有属性的提案 
目前，有一个提案，为class加了私有属性。方法是在属性名之前，使用#表示。
```js
class IncreasingCounter {
  #count = 0;
  get value() {
    console.log('Getting the current value!');
    return this.#count;
  }
  increment() {
    this.#count++;
  }
}
```
上面代码中，#count就是私有属性，只能在类的内部使用（this.#count）。如果在类的外部使用，就会报错。
> 由于井号#是属性名的一部分，使用时必须带有#一起使用，所以#x和x是两个不同的属性。
```js
class Foo {
  #a;
  #b;
  constructor(a, b) {
    this.#a = a;
    this.#b = b;
  }
  #sum() {
    return this.#a + this.#b;
  }
  printSum() {
    console.log(this.#sum());
  }
}
```
上面代码中，#sum()就是一个私有方法。
另外，私有属性也可以设置 getter 和 setter 方法。
```js
class Counter {
  #xValue = 0;

  constructor() {
    super();
    // ...
  }

  get #x() { return #xValue; }
  set #x(value) {
    this.#xValue = value;
  }
}
```
私有属性和私有方法前面，也可以加上static关键字，表示这是一个静态的私有属性或私有方法。
```js
class FakeMath {
  static PI = 22 / 7;
  static #totallyRandomNumber = 4;

  static #computeRandomNumber() {
    return FakeMath.#totallyRandomNumber;
  }

  static random() {
    console.log('I heard you like random numbers…')
    return FakeMath.#computeRandomNumber();
  }
}

FakeMath.PI // 3.142857142857143
FakeMath.random()
// I heard you like random numbers…
// 4
FakeMath.#totallyRandomNumber // 报错
FakeMath.#computeRandomNumber() // 报错
```
上面代码中，#totallyRandomNumber是私有属性，#computeRandomNumber()是私有方法，只能在FakeMath这个类的内部调用，外部调用就会报错。
<hr>

### 6.new.target 属性
new是从构造函数生成实例对象的命令。ES6 为new命令引入了一个new.target属性，该属性一般用在构造函数之中，返回new命令作用于的那个构造函数。如果构造函数不是通过new命令或Reflect.construct()调用的，new.target会返回undefined，因此这个属性可以用来确定构造函数是怎么调用的。
```js
function Person(name) {
  if (new.target !== undefined) {
    this.name = name;
  } else {
    throw new Error('必须使用 new 命令生成实例');
  }
}

// 另一种写法
function Person(name) {
  if (new.target === Person) {
    this.name = name;
  } else {
    throw new Error('必须使用 new 命令生成实例');
  }
}

var person = new Person('张三'); // 正确
var notAPerson = Person.call(person, '张三');  // 报错
```
上面代码确保构造函数只能通过new命令调用。

Class 内部调用new.target，返回当前 Class。

```js
class Rectangle {
  constructor(length, width) {
    console.log(new.target === Rectangle);
    this.length = length;
    this.width = width;
  }
}

var obj = new Rectangle(3, 4); // 输出 true
```
需要注意的是，子类继承父类时，new.target会返回子类。
```js
class Rectangle {
  constructor(length, width) {
    console.log(new.target === Rectangle);
    // ...
  }
}

class Square extends Rectangle {
  constructor(length, width) {
    super(length, width);
  }
}

var obj = new Square(3); // 输出 false
```
上面代码中，new.target会返回子类。

利用这个特点，可以写出不能独立使用、必须继承后才能使用的类。


```js
class Shape {
  constructor() {
    if (new.target === Shape) {
      throw new Error('本类不能实例化');
    }
  }
}

class Rectangle extends Shape {
  constructor(length, width) {
    super();
    // ...
  }
}

var x = new Shape();  // 报错
var y = new Rectangle(3, 4);  // 正确
```
上面代码中，Shape类不能被实例化，只能用于继承。

注意，在函数外部，使用new.target会报错。

### 总结
#### 基本语法
**简介**
- 1.constructor方法是类默认方法，通过new命令生成对象实例时，自动调用该方法。一个类必须有constructor方法，如果没有显示定义，一个空的constructor方法会被默认添加
- 2.使用class时不加new会报错
- 3.实例的属性除非显式定义在其本身（即定义在this对象上），否则都是定义在原型上（即定义在class上）。
- 4.在“类”的内部可以使用get和set关键字，对某个属性设置存值函数和取值函数，拦截该属性的存取行为。
- 5.类的方法内部如果含有this，它默认指向类的实例。

**静态方法**
- 6.如果在一个方法前，加上static关键字，就表示该方法不会被实例继承，而是直接通过类来调用，这就称为“静态方法”。
- 7.如果静态方法包含this关键字，这个this指的是类，而不是实例。

**实例属性的新写法**
- 8.属性也可以定义在类的最顶层

**静态属性**
- 9.写法是在实例属性的前面，加上static关键字。

**私有方法和私有属性**
- 10.为class加了私有属性。方法是在属性名之前，使用#表示。
- 11.#count就是私有属性，只能在类的内部使用（this.#count）。如果在类的外部使用，就会报错。
- 12.#sum()就是一个私有方法。
- 13.另外，私有属性也可以设置 getter 和 setter 方法。

**new.target 属性**
- 14.new命令引入了一个new.target属性，该属性一般用在构造函数之中，返回new命令作用于的那个构造函数。如果构造函数不是通过new命令或Reflect.construct()调用的，new.target会返回undefined，因此这个属性可以用来确定构造函数是怎么调用的。
- 15.需要注意的是，子类继承父类时，new.target会返回子类。
- 16.利用这个特点，可以写出不能独立使用、必须继承后才能使用的类。(类似抽象类)

## class的继承
### 1.简介
Class 可以通过extends关键字实现继承，这比 ES5 的通过修改原型链实现继承，要清晰和方便很多。
```js
class Point {
}

class ColorPoint extends Point {
}
```
```js
class ColorPoint extends Point {
  constructor(x, y, color) {
    super(x, y); // 调用父类的constructor(x, y)
    this.color = color;
  }

  toString() {
    return this.color + ' ' + super.toString(); // 调用父类的toString()
  }
}
```
上面代码中，constructor方法和toString方法之中，都出现了super关键字，它在这里表示父类的构造函数，用来新建父类的this对象。

子类必须在constructor方法中调用super方法，否则新建实例时会报错。这是因为子类自己的this对象，必须先通过父类的构造函数完成塑造，得到与父类同样的实例属性和方法，然后再对其进行加工，加上子类自己的实例属性和方法。如果不调用super方法，子类就得不到this对象.

```js
class Point { /* ... */ }

class ColorPoint extends Point {
  constructor() {
  }
}

let cp = new ColorPoint(); // ReferenceError
```
上面代码中，ColorPoint继承了父类Point，但是它的构造函数没有调用super方法，导致新建实例时报错。

ES5 的继承，<font color="red">实质是先创造子类的实例对象this，然后再将父类的方法添加到this上面（Parent.apply(this)）。</font>
ES6 的继承机制完全不同，**实质是先将父类实例对象的属性和方法，加到this上面（所以必须先调用super方法），然后再用子类的构造函数修改this。**

如果子类没有定义constructor方法，这个方法会被默认添加，代码如下。也就是说，不管有没有显式定义，任何一个子类都有constructor方法。
```js
class ColorPoint extends Point {
}

// 等同于
class ColorPoint extends Point {
  constructor(...args) {
    super(...args);
  }
}
```
另一个需要注意的地方是，在子类的构造函数中，只有调用super之后，才可以使用this关键字，否则会报错。这是因为子类实例的构建，基于父类实例，只有super方法才能调用父类实例。
```js
class Point {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }
}

class ColorPoint extends Point {
  constructor(x, y, color) {
    this.color = color; // ReferenceError
    super(x, y);
    this.color = color; // 正确
  }
}
```
上面代码中，子类的constructor方法没有调用super之前，就使用this关键字，结果报错，而放在super方法之后就是正确的。

最后，父类的静态方法，也会被子类继承。
```js
class A {
  static hello() {
    console.log('hello world');
  }
}

class B extends A {
}

B.hello()  // hello world
```
### 2.Object.getPrototypeOf()
Object.getPrototypeOf方法可以用来从子类上获取父类。
```js
Object.getPrototypeOf(ColorPoint) === Point
// true
```
因此，可以使用这个方法判断，一个类是否继承了另一个类。
<hr>

### 3.super 关键字
super这个关键字，既可以当作函数使用，也可以当作对象使用。在这两种情况下，它的用法完全不同。

**第一种情况，super作为函数调用时，代表父类的构造函数。ES6 要求，子类的构造函数必须执行一次super函数。**
```js
class A {}

class B extends A {
  constructor() {
    super();
  }
}
```
上面代码中，子类B的构造函数之中的super()，代表调用父类的构造函数。这是必须的，否则 JavaScript 引擎会报错。
<font color="red" style="font-weight:bold;">注意，super虽然代表了父类A的构造函数，但是返回的是子类B的实例，即super内部的this指的是B的实例，因此super()在这里相当于A.prototype.constructor.call(this).</font>

作为函数时，super()只能用在子类的构造函数之中，用在其他地方就会报错。
```js
class A {}

class B extends A {
  m() {
    super(); // 报错
  }
}
```
**第二种情况，super作为对象时，在普通方法中，指向父类的原型对象；在静态方法中，指向父类。**
```js
class A {
  p() {
    return 2;
  }
}

class B extends A {
  constructor() {
    super();
    console.log(super.p()); // 2   普通函数，super指向父类原型对象
  }
}

let b = new B();
```
super在普通方法之中，指向A.prototype，所以super.p()就相当于A.prototype.p()。

这里需要注意，由于super指向父类的原型对象，所以定义在父类实例上的方法或属性，是无法通过super调用的。
```js
class A {
  constructor() {
    this.p = 2;   //父类实例上的属性
  }
}

class B extends A {
  get m() {
    return super.p;
  }
}

let b = new B();
b.m // undefined
```
上面代码中，p是父类A实例的属性，super.p就引用不到它。
**ES6 规定，在子类普通方法中通过super调用父类的方法时，方法内部的this指向当前的子类实例。**
```js
class A {
  constructor() {
    this.x = 1;
  }
  print() {
    console.log(this.x);
  }
}

class B extends A {
  constructor() {
    super();
    this.x = 2;
  }
  m() {
    super.print();
  }
}

let b = new B();
b.m() // 2
```
上面代码中，super.print()虽然调用的是A.prototype.print()，但是A.prototype.print()内部的this指向子类B的实例，导致输出的是2，而不是1。也就是说，实际上执行的是super.print.call(this)。
**如果super作为对象，用在静态方法之中，这时super将指向父类，而不是父类的原型对象。**
```js
class Parent {
  static myMethod(msg) {
    console.log('static', msg);
  }

  myMethod(msg) {
    console.log('instance', msg);
  }
}

class Child extends Parent {
  static myMethod(msg) {
    super.myMethod(msg);
  }

  myMethod(msg) {
    super.myMethod(msg);
  }
}

Child.myMethod(1); // static 1

var child = new Child();
child.myMethod(2); // instance 2
```
注意，使用super的时候，必须显式指定是作为函数、还是作为对象使用，否则会报错。

```js
class A {}

class B extends A {
  constructor() {
    super();
    console.log(super); // 报错
  }
}
```
上面代码中，console.log(super)当中的super，无法看出是作为函数使用，还是作为对象使用，所以 JavaScript 引擎解析代码的时候就会报错。这时，如果能清晰地表明super的数据类型，就不会报错。
```js
class A {}

class B extends A {
  constructor() {
    super();
    console.log(super.valueOf() instanceof B); // true
  }
}

let b = new B();
```
上面代码中，super.valueOf()表明super是一个对象，因此就不会报错。同时，由于super使得this指向B的实例，所以super.valueOf()返回的是一个B的实例。
<hr>

### 4.类的 prototype 属性和__proto__属性
大多数浏览器的 ES5 实现之中，每一个对象都有__proto__属性，指向对应的构造函数的prototype属性。Class 作为构造函数的语法糖，同时有prototype属性和__proto__属性，因此同时存在两条继承链。
（1）子类的__proto__属性，表示构造函数的继承，总是指向父类。

（2）子类prototype属性的__proto__属性，表示方法的继承，总是指向父类的prototype属性。
```js
class A {
}

class B extends A {
}

B.__proto__ === A // true
B.prototype.__proto__ === A.prototype // true
```
上面代码中，子类B的__proto__属性指向父类A，子类B的prototype属性的__proto__属性指向父类A的prototype属性。

这样的结果是因为，类的继承是按照下面的模式实现的。
```js
class A {
}

class B {
}

// B 的实例继承 A 的实例
Object.setPrototypeOf(B.prototype, A.prototype);

// B 继承 A 的静态属性
Object.setPrototypeOf(B, A);

const b = new B();

```
《对象的扩展》一章给出过Object.setPrototypeOf方法的实现。
```js
Object.setPrototypeOf = function (obj, proto) {
  obj.__proto__ = proto;
  return obj;
}
```
因此，就得到了上面的结果。

```js
Object.setPrototypeOf(B.prototype, A.prototype);
// 等同于
B.prototype.__proto__ = A.prototype;

Object.setPrototypeOf(B, A);
// 等同于
B.__proto__ = A;
```
<hr>

### 5.原生构造函数的继承
原生构造函数是指语言内置的构造函数，通常用来生成数据结构。ECMAScript 的原生构造函数大致有下面这些。
- Boolean()
- Number()
- String()
- Array()
- Date()
- Function()
- RegExp()
- Error()
- Object()
ES6 允许继承原生构造函数定义子类，因为 ES6 是先新建父类的实例对象this，然后再用子类的构造函数修饰this，使得父类的所有行为都可以继承。下面是一个继承Array的例子。
```js
class MyArray extends Array {
  constructor(...args) {
    super(...args);
  }
}

var arr = new MyArray();
arr[0] = 12;
arr.length // 1

arr.length = 0;
arr[0] // undefined

```
> 注意，继承Object的子类，有一个行为差异。
```js
class NewObj extends Object{
  constructor(){
    super(...arguments);
  }
}
var o = new NewObj({attr: true});
o.attr === true  // false
```
上面代码中，NewObj继承了Object，但是无法通过super方法向父类Object传参。这是因为 ES6 改变了Object构造函数的行为，一旦发现Object方法不是通过new Object()这种形式调用，ES6 规定Object构造函数会忽略参数。




### 总结
#### 继承
**简介**
- 1.Class 可以通过extends关键字实现继承
- 2.子类必须在constructor方法中调用super方法，否则新建实例时会报错。这是因为子类自己的this对象，必须先通过父类的构造函数完成塑造，得到与父类同样的实例属性和方法，然后再对其进行加工，加上子类自己的实例属性和方法。如果不调用super方法，子类就得不到this对象。
- 3.ES5 的继承，实质是先创造子类的实例对象this，然后再将父类的方法添加到this上面（Parent.apply(this)）。ES6 的继承机制完全不同，实质是先将父类实例对象的属性和方法，加到this上面（所以必须先调用super方法），然后再用子类的构造函数修改this。
- 4.另一个需要注意的地方是，在子类的构造函数中，只有调用super之后，才可以使用this关键字，否则会报错。这是因为子类实例的构建，基于父类实例，只有super方法才能调用父类实例。

**Object.getPrototypeOf()**
- 5.Object.getPrototypeOf方法可以用来从子类上获取父类。

**super 关键字**
- 6.第一种情况，super作为函数调用时，代表父类的构造函数。ES6 要求，子类的构造函数必须执行一次super函数。
- 7.super()在这里相当于A.prototype.constructor.call(this)
- 8.第二种情况，super作为对象时，在普通方法中，指向父类的原型对象；在静态方法中，指向父类。

**类的 prototype 属性和__proto__属性**
- 9.子类的__proto__属性，表示构造函数的继承，总是指向父类。
- 10.子类prototype属性的__proto__属性，表示方法的继承，总是指向父类的prototype属性。
- 11.可以理解为__proto__总是指向同一类

**原生构造函数的继承**
- 12.extends关键字不仅可以用来继承类，还可以用来继承原生的构造函数。



## encodeURI、encodeURIComponent、decodeURI、decodeURIComponent的区别
![不同](https://lc-gold-cdn.xitu.io/6f5dace798f2d7763cc9.png?imageslim)