# vuepress-blog（第三方博客）
> a vuepress blog about Janzenkin

### Build Setup

# clone item
git clone git@github.com:qiufeihong2018/vuepress-blog.git

# install dependencies
yarn install

# serve with hot reload at localhost:8080
yarn docs:dev

# build for production with minification
yarn docs:build


### main page
![avatar](./shotPic/main.png)

### feature
- [x] 可以统计阅读量
- [x] 支持评论

